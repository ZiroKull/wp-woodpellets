<?php get_header('old'); ?>

    <div class="col-md-12 col-sm-12 col-xs-12 clear-pads dashboard-template">

        <?php get_template_part('partials/dashboard/side-menu');?>

        <div class="content-wrap col-md-12 col-sm-12 col-xs-12 extended">

            <?php get_template_part('partials/dashboard/header');?>

            <div class="col-md-12 col-sm-12 col-xs-12 dashboard dashboard-club">
			
				<div class="container">
			
					<h3><b><?php echo __( 'Новости', 'preico' ) ?></b></h3>
					
					<?php
						$category = get_category( get_query_var( 'cat' ) );

						$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
						$query = new WP_Query( 
							array(
								'paged'         => $paged, 
								'category_name' => $category->slug,
								'order'         => 'asc',
								'post_type'     => 'post',
								'post_status'   => 'publish',
							)
						);
						if ($query->have_posts()) :
							while ($query->have_posts()) : 
								$query->the_post();
								
								global $post;
						?>
						
							<article id="post-<?php the_ID(); ?>" <?php post_class('post-box small-post-box'); ?>>
								
								<div class="post-img small-post-img">	
									<a href="<?php echo site_url(); ?><?php echo wpml_site_link(); ?><?php echo $category->slug; ?>/<?php the_ID(); ?>-<?php echo $post->post_name; ?>">
										<?php
											if ( ! has_post_thumbnail() ) 
											{
										?>
												<img src="<?php bloginfo('template_directory');?>/img/default.png" class="img-thumbnail" width="250" height="150">
										<?php
											}
											else 
											{									
												the_post_thumbnail( 'post-thumbnail img-thumbnail', array( 'alt' => get_the_title() ) );
											}
										?>
										
										<div class="post-format"><i class="fa fa-file-text"></i></div>
									</a>
								</div>
								
								<div class="post-data small-post-data">
									<div class="post-data-container">
										<header class="entry-header">
											<h2 class="entry-title post-title">
												<a href="<?php echo site_url(); ?><?php echo wpml_site_link(); ?><?php echo $category->slug; ?>/<?php the_ID(); ?>-<?php echo $post->post_name; ?>" rel="bookmark"><?php the_title( ); ?></a>
											</h2>									
										</header><!-- .entry-header -->
										
										<div class="entry-content post-excerpt">
											<?php the_content(); ?>
										</div><!-- .entry-content -->
										
										<div class="readmore">
											<a href="<?php echo site_url(); ?><?php echo wpml_site_link(); ?><?php echo $category->slug; ?>/<?php the_ID(); ?>-<?php echo $post->post_name; ?>"><?php echo __( 'Читать далее', 'preico' ) ?></a>
										</div>
									</div>
								</div>

							</article><!-- #post-## -->
						
						<?php
						// End the loop.
						endwhile;
						
						wp_pagenavi();
						
						wp_reset_postdata();

					endif;
					?>
				</div>
            </div>
        </div>

    </div>

    <div class="dashboard-modals">
        <?php get_template_part('partials/dashboard/user-edit-form');?>
    </div>

	<!-- jQuery first, then Tether, then Bootstrap JS. -->
	<script src="<?php bloginfo('template_directory');?>/js/jquery-1.11.1.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/bootstrap.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/validator/formValidation.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/validator/bootstrap-validator.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/dashboard.js"></script>
