
	<!-- Section Footer -->
	<footer class="section-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-8 margin-t-40">
					<div class="logo-wrap">
						<img src="<?php bloginfo('template_directory');?>/img/logo.png" alt="">
					</div>
					
					
					<ul class="hidden-xs">
					
						<?php
							$menu_locations = get_nav_menu_locations();
							$menu = $menu_locations[ 'footer' ];
							$menu = wp_get_nav_menu_items( $menu );
							
							$num = 0;
							
							foreach ( $menu as $item )
							{
								$current = ( $item->object_id == get_queried_object_id() ) ? 'menu-item-active' : '';
								
								if (is_user_logged_in() && ($num === 5))
								{
									$num++;
									continue;
								}
								
								if (!is_user_logged_in() && ($num === 6))
								{
									$num++;
									continue;
								}
						?>
							<li class="<?php echo $current; ?>">
								<a href="<?php echo $item->url;?>"><?php echo $item->title;?></a>
							</li>
						<?php 					
								$num++;
							} 
						?>
						
					</ul>
					
					<p class="copyright"><?php echo __( 'Copyright © ПРОЕКТ "WOOD PELLETS"', 'preico' ) ?></p>
				</div>
				
				<div class="col-md-3 col-md-offset-1 margin-t-65 adress">
					<p><?php echo __( 'ООО «Ланган»', 'preico' ) ?></p>
					<p><?php echo __( '182250, Псковская обл., Себежский р-н', 'preico' ) ?></p>
					<p><?php echo __( 'г. Себеж, ул. В.И. Марго, д. 32', 'preico' ) ?></p>
					<p><?php echo __( 'ОГРН 1136027000262', 'preico' ) ?></p>
					<p><?php echo __( 'ИНН/КПП 6027147058 602201001', 'preico' ) ?></p>
				</div>
			</div>
		</div>
	</footer><!-- /.section-footer -->

	<?php wp_footer(); ?>
	
	<script>
		var domain = '<?php echo get_site_url(); ?>';
	</script>

	<!-- jQuery first, then Tether, then Bootstrap JS. -->
	<script src="<?php bloginfo('template_directory');?>/js/bootstrap.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/club.js"></script>
	
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript" >
		(function (d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter47970584 = new Ya.Metrika({
						id:47970584,
						clickmap:true,
						trackLinks:true,
						accurateTrackBounce:true,
						webvisor:true
					});
				} catch(e) { }
			});

			var n = d.getElementsByTagName("script")[0],
				s = d.createElement("script"),
				f = function () { n.parentNode.insertBefore(s, n); };
			s.type = "text/javascript";
			s.async = true;
			s.src = "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else { f(); }
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript><div><img src="https://mc.yandex.ru/watch/47970584" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->

</body>
</html>