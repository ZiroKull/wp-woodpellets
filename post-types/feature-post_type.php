<?php

add_action( 'init', 'ico_feature' );
/**
 * Register a event post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function ico_feature() {
    $labels = array(
        'name'               => 'Особеность',
        'singular_name'      => 'Особеность',
        'add_new'            => 'Добавить',
        'add_new_item'       => 'Добавить',
        'edit_item'          => 'Редактировать',
        'new_item'           => 'Новый',
        'view_item'          => 'Просмотреть',
        'search_items'       => 'Поиск',
        'not_found'          => __( 'Ничего не найдено', 'preico' ),
        'not_found_in_trash' => __( 'Корзина пуста', 'preico' ),
        'parent_item_colon'  => ''
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'feature' ),
        'capability_type'    => 'post',
        'hierarchical'       => true,
        'menu_position'      => 42,
        'supports'           => array( 'title', 'editor', 'thumbnail' )
    );

    register_post_type( 'feature', $args );

}
