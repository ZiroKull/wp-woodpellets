<?php

add_filter('show_admin_bar', '__return_false');
require 'post-types/team-post_type.php';
require 'post-types/partner-post_type.php';
require 'post-types/questions-post_type.php';
require 'post-types/feature-post_type.php';
require 'post-types/infographic-post_type.php';
require 'post-types/club-post_type.php';

add_action( 'after_setup_theme', 'my_theme_setup' );
function my_theme_setup()
{
	load_theme_textdomain( 'preico', get_template_directory() . '/lang' );
}

add_theme_support( 'post-thumbnails' );

add_filter('single_template', 'check_for_category_single_template', 0);
function check_for_category_single_template( $t )
{
	$slugs = explode('/', get_query_var('category_name'));
	$currentCategory = get_category_by_slug('/'.end($slugs));

	if ( file_exists(TEMPLATEPATH . "/single-category-{$currentCategory->slug}.php") ) return TEMPLATEPATH . "/single-category-{$currentCategory->slug}.php"; 

	return $t;
}

function debug($value)
{
	echo '<pre>';
	print_r($value);
	echo '</pre>';
}

if ( !function_exists( 'wp_password_change_notification' ) ) {
    function wp_password_change_notification() {}
}

if( !function_exists('wp_password_change_notification') ) {
    function  wp_new_user_notification(){}
}


add_filter( 'send_email_change_email', '__return_false' );

function theme_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'preico' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'preico' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'theme_widgets_init' );

if (function_exists('register_sidebar')) {

	register_sidebar(array(
		'name' => 'Footer',
		'id'   => 'footer',
		'description'   => 'This is the widgetized footer.',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>'
	));

	 register_sidebar( array(
        'name' => __( 'Main Sidebar', 'preico' ),
        'id' => 'sidebar-right',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widgettitle">',
		'after_title'   => '</h4>',
    ) );
}

if ( ! function_exists( 'twentyfifteen_post_thumbnail' ) ) :
/**
 * Display an optional post thumbnail.
 *
 * Wraps the post thumbnail in an anchor element on index views, or a div
 * element when on single views.
 *
 * @since Twenty Fifteen 1.0
 */
function twentyfifteen_post_thumbnail() {
	if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
		return;
	}

	if ( is_singular() ) :
	?>

	<div class="post-thumbnail">
		<?php the_post_thumbnail(); ?>
	</div><!-- .post-thumbnail -->

	<?php else : ?>

	<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
		<?php
			the_post_thumbnail( 'post-thumbnail', array( 'alt' => get_the_title() ) );
		?>
	</a>

	<?php endif; // End is_singular()
}
endif;

function randomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function update_user(){

    //$user = $_POST;
    //debug($user);

    $wpcrl_fname = $_POST[ 'wpcrl_fname' ];
    $wpcrl_lname = $_POST[ 'wpcrl_lname' ];
    $wpcrl_email = $_POST[ 'wpcrl_email' ];
    $wpcrl_password = $_POST[ 'wpcrl_new_password' ];
	$wpcrl_wallet = $_POST[ 'wpcrl_wallet' ];


    $data = [ 'ID' => get_current_user_id() ];

    if( $wpcrl_email != '' ){
        $data[ 'user_email' ] = $wpcrl_email;
    }
	
    if( $wpcrl_password != '' ){
        $data[ 'user_pass' ] = $wpcrl_password;
    }


    update_user_meta( get_current_user_id(), 'first_name', $wpcrl_fname );
    update_user_meta( get_current_user_id(), 'last_name', $wpcrl_lname );
	update_user_meta( get_current_user_id(), 'wpcrl_wallet', $wpcrl_wallet );

    wp_update_user( $data );

    echo 1;
    die();
}
add_action( 'wp_ajax_update_user', 'update_user' );
add_action( 'wp_ajax_nopriv_update_user', 'update_user' );

function getTokenBalance()
{

	if (null !== get_user_meta( get_current_user_id(), 'wpcrl_wallet', true ))
	{
		$response = wp_remote_get( get_site_url() . '/eth/token.php?tokenAddr=' . get_user_meta( get_current_user_id(), 'wpcrl_wallet', true ) );

		update_user_meta( get_current_user_id(), 'wpcrl_user_balance', $response['body'] );
		
		if ((String)$response['body'] > 500)
		{
			update_user_meta( get_current_user_id(), 'wpcrl_user_status', '2' );
		}
		
		if ((String)$response['body'] > 5000)
		{
			update_user_meta( get_current_user_id(), 'wpcrl_user_status', '3' );
		}
		
		if ((String)$response['body'] > 50000)
		{
			update_user_meta( get_current_user_id(), 'wpcrl_user_status', '4' );
		}
		
		if ((String)$response['body'] > 500000)
		{
			update_user_meta( get_current_user_id(), 'wpcrl_user_status', '5' );
		}
		
		if ((String)$response['body'] < 500)
		{
			update_user_meta( get_current_user_id(), 'wpcrl_user_status', '1' );
		}
		
		//var_dump( $response['body'] ) ; 
		
		return (String)$response['body'] ; 
	}
	else 
	{
		update_user_meta( get_current_user_id(), 'wpcrl_user_status', '1' );
		update_user_meta( get_current_user_id(), 'wpcrl_user_balance', 0 );
	}
	
	return 0;
}

function check_countdown() 
{
	echo get_option( 'data_end_ico' );
	die();
}

add_action( 'wp_ajax_check_countdown', 'check_countdown' );
add_action( 'wp_ajax_nopriv_check_countdown', 'check_countdown' );

add_action('admin_menu', 'ico_create_menu');

function add_e2_date_picker()
{
	//jQuery UI date picker file
	wp_enqueue_script('jquery-ui-datepicker');
	//jQuery UI theme css file
	wp_enqueue_style('e2b-admin-ui-css','https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css',false,"1.9.0",false);
}

add_action('admin_enqueue_scripts', 'add_e2_date_picker'); 

function ico_create_menu() 
{
	//create new top-level menu
	add_menu_page('Настройки Краудсейла', 'Настройки Краудсейла', 'administrator', __FILE__, 'ico_settings_page');

	//call register settings function
	add_action( 'admin_init', 'register_icosettings' );
}


function register_icosettings() 
{
	//register our settings
	register_setting( 'ico-settings-group', 'data_end_ico' );
	register_setting( 'ico-settings-group', 'phone_ico' );
	register_setting( 'ico-settings-group', 'vk_link_ico' );
	register_setting( 'ico-settings-group', 'fb_link_ico' );
	register_setting( 'ico-settings-group', 'tl_link_ico' );
	register_setting( 'ico-settings-group', 'tw_link_ico' );
	register_setting( 'ico-settings-group', 'md_link_ico' );
}

function ico_settings_page() 
{
	?>
	<div class="wrap">
		<h2>Настройки Краудсейла!</h2>

		<form method="post" action="options.php">
			<?php settings_fields( 'ico-settings-group' ); ?>
			<table class="form-table">
				<tr valign="top">
					<th scope="row"><?php _e('Дата (Завершения ICO)') ?></th>
					<td><input type="text" class="datepicker" name="data_end_ico" value="<?php echo get_option('data_end_ico'); ?>" /></td>
				</tr>
				<tr valign="top">
					<th scope="row"><?php _e('Телефон') ?></th>
					<td><input type="text" name="phone_ico" value="<?php echo get_option('phone_ico'); ?>" /></td>
				</tr>
				<tr valign="top">
					<th scope="row"><?php _e('Vkontacte') ?></th>
					<td><input type="text" name="vk_link_ico" value="<?php echo get_option('vk_link_ico'); ?>" /></td>
				</tr>
				<tr valign="top">
					<th scope="row"><?php _e('Facebook') ?></th>
					<td><input type="text" name="fb_link_ico" value="<?php echo get_option('fb_link_ico'); ?>" /></td>
				</tr>
				<tr valign="top">
					<th scope="row"><?php _e('Telegram') ?></th>
					<td><input type="text" name="tl_link_ico" value="<?php echo get_option('tl_link_ico'); ?>" /></td>
				</tr>
				<tr valign="top">
					<th scope="row"><?php _e('Twitter') ?></th>
					<td><input type="text" name="tw_link_ico" value="<?php echo get_option('tw_link_ico'); ?>" /></td>
				</tr>
				<tr valign="top">
					<th scope="row"><?php _e('Medium') ?></th>
					<td><input type="text" name="md_link_ico" value="<?php echo get_option('md_link_ico'); ?>" /></td>
				</tr>
			</table>
			
			<p class="submit">
			<input type="submit" class="button-primary" value="<?php _e('Сохранить') ?>" />
			</p>

		</form>
	</div>
	
	<script>
    jQuery(function() {
        jQuery( ".datepicker" ).datepicker({
            dateFormat : "dd-mm-yy"
        });
    });
    </script> 
	<?php 
} 


/*
function get_user_data()
{
    //meta
    $name = get_user_meta( get_current_user_id(), 'first_name' );
    $surname = get_user_meta( get_current_user_id(), 'last_name' );
    $nick_name = get_user_meta( get_current_user_id(), 'nickname' );

    $user_data = get_userdata( get_current_user_id() );


    $data = [
        'name'          => $name[0],
        'surname'       => $surname[0],
        'nickname'      => $user_data->user_login,
        'email'         => $user_data->user_email
    ];
    json_response($data);
    die();
}
add_action( 'wp_ajax_get_user_data', 'get_user_data' );
add_action( 'wp_ajax_nopriv_get_user_data', 'get_user_data' );*/

add_action('init', function(){

	global $wpdb, $PasswordHash, $current_user, $user_ID;
	
	$err = '';
	$success = '';

	if(isset($_POST['task']) && $_POST['task'] == 'login' )
	{
		//We shall SQL escape all inputs to avoid sql injection.
		$username = $wpdb->escape($_POST['Email']);
		$password = $wpdb->escape($_POST['Password']);
		$remember = $wpdb->escape($_POST['RememberMe']);
		
		if (filter_var($username, FILTER_VALIDATE_EMAIL)) //Invalid Email
		{ 
			$user = get_user_by('email', $username);
		} else {
			$user = get_user_by('login', $username);
		}
		 
		if ($user && wp_check_password( $password, $user->data->user_pass, $user->ID))
		{
			$creds = array('user_login' => $user->data->user_login, 'user_password' => $password, 'remember' => $remember);
			$user2 = wp_signon( $creds, is_ssl() );
			
			wp_set_auth_cookie($user2->data->ID);
			wp_set_current_user($user2->data->ID, $user2->data->user_login);
			
			do_action('set_current_user');
			
			header('Location: ' . "https://woodpellets.io/private-office/");
		}
	}

	if(isset($_POST['task']) && $_POST['task'] == 'register' )
	{
		$email = $wpdb->escape(trim($_POST['Email']));
		$pwd1 = $wpdb->escape(trim($_POST['Password']));
		
		if(email_exists($email) ) 
		{
			$err = 'Email уже зарегистрирован.';
		}
		else 
		{
			$userdata = array(
				'user_login' => apply_filters('pre_user_login', $email),
                'user_pass' => apply_filters('pre_user_pass', $pwd1),
                'user_email' => apply_filters('pre_user_email', $email),
                'role' => get_option('default_role'),
                'user_registered' => date('Y-m-d H:i:s')
            );
			
			$user_id = wp_insert_user($userdata);
			
			 // checking for errors while user registration
            if (is_wp_error($user_id)) 
			{
                $err = $user_id->get_error_message();
            } 
			else 
			{
				do_action('user_register', $user_id);
				
				$user  = get_user_by('email', $email);
				$creds = array('user_login' => $user->data->user_login, 'user_password' => $pwd1);
				$user2 = wp_signon( $creds, is_ssl() );
			
				wp_set_auth_cookie($user2->data->ID);
				wp_set_current_user($user2->data->ID);
				
				do_action('set_current_user');
				
				header('Location: ' . "https://woodpellets.io/private-office/");
			}
		}
	}	
});

function my_flag_only_language_switcher() 
{
    $languages = apply_filters( 'wpml_active_languages', NULL, 'orderby=id&order=desc' );
 
    if ( !empty( $languages ) ) 
	{
		echo '<li>';
		echo 	'<div class="dropdown">';
		
		foreach( $languages as $l ) 
		{
			if ( $l['active'] )
			{
				echo '<button class="dropdown-toggle" type="button" data-toggle="dropdown">';
				echo 	'<img src="' . $l['country_flag_url'] . '" height="12" alt="' . $l['language_code'] . '" width="18" />';
				echo 	'<span class="locale">' . $l['language_code'] . '</span>';
				echo 	'<span class="caret"></span>';
				echo '</button>';
			}
		}
		
		echo '<ul class="dropdown-menu">';
		
        foreach( $languages as $l ) 
		{
            if ( !$l['active'] )
			{
				echo '<li>';
				echo 	'<a href="' . $l['url'] . '">';
				echo 		'<img src="' . $l['country_flag_url'] . '" height="12" alt="' . $l['language_code'] . '" width="18" />';
				echo 		'<span class="locale">' . $l['language_code'] . '</span>';
				echo 	'</a>';
				echo '</li>';
			}
        }
		
		echo '</ul>';
		
		echo 	'</div>';
		echo '</li>';
    }
}

function wpml_site_link()
{
	$dl = acf_get_setting('default_language');
	$cl = acf_get_setting('current_language');
	
	if ($dl !== $cl)
		return '/ru/';
	
	return '/';
}

/*
 * Функция создает дубликат поста в виде черновика и редиректит на его страницу редактирования
 */
function true_duplicate_post_as_draft(){
	global $wpdb;
	if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'true_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
		wp_die('Нечего дублировать!');
	}
 
	/*
	 * получаем ID оригинального поста
	 */
	$post_id = (isset($_GET['post']) ? $_GET['post'] : $_POST['post']);
	/*
	 * а затем и все его данные
	 */
	$post = get_post( $post_id );
 
	/*
	 * если вы не хотите, чтобы текущий автор был автором нового поста
	 * тогда замените следующие две строчки на: $new_post_author = $post->post_author;
	 * при замене этих строк автор будет копироваться из оригинального поста
	 */
	$current_user = wp_get_current_user();
	$new_post_author = $current_user->ID;
 
	/*
	 * если пост существует, создаем его дубликат
	 */
	if (isset( $post ) && $post != null) {
 
		/*
		 * массив данных нового поста
		 */
		$args = array(
			'comment_status' => $post->comment_status,
			'ping_status'    => $post->ping_status,
			'post_author'    => $new_post_author,
			'post_content'   => $post->post_content,
			'post_excerpt'   => $post->post_excerpt,
			'post_name'      => $post->post_name,
			'post_parent'    => $post->post_parent,
			'post_password'  => $post->post_password,
			'post_status'    => 'draft', // черновик, если хотите сразу публиковать - замените на publish
			'post_title'     => $post->post_title,
			'post_type'      => $post->post_type,
			'to_ping'        => $post->to_ping,
			'menu_order'     => $post->menu_order
		);
 
		/*
		 * создаем пост при помощи функции wp_insert_post()
		 */
		$new_post_id = wp_insert_post( $args );
 
		/*
		 * присваиваем новому посту все элементы таксономий (рубрики, метки и т.д.) старого
		 */
		$taxonomies = get_object_taxonomies($post->post_type); // возвращает массив названий таксономий, используемых для указанного типа поста, например array("category", "post_tag");
		foreach ($taxonomies as $taxonomy) {
			$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
			wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
		}
 
		/*
		 * дублируем все произвольные поля
		 */
		$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
		if (count($post_meta_infos)!=0) {
			$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
			foreach ($post_meta_infos as $meta_info) {
				$meta_key = $meta_info->meta_key;
				$meta_value = addslashes($meta_info->meta_value);
				$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
			}
			$sql_query.= implode(" UNION ALL ", $sql_query_sel);
			$wpdb->query($sql_query);
		}
 
 
		/*
		 * и наконец, перенаправляем пользователя на страницу редактирования нового поста
		 */
		wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
		exit;
	} else {
		wp_die('Ошибка создания поста, не могу найти оригинальный пост с ID=: ' . $post_id);
	}
}
add_action( 'admin_action_true_duplicate_post_as_draft', 'true_duplicate_post_as_draft' );
 
/*
 * Добавляем ссылку дублирования поста для post_row_actions
 */
function true_duplicate_post_link( $actions, $post ) {
	if (current_user_can('edit_posts')) {
		$actions['duplicate'] = '<a href="admin.php?action=true_duplicate_post_as_draft&amp;post=' . $post->ID . '" title="Дублировать этот пост" rel="permalink">Дублировать</a>';
	}
	return $actions;
}
 
add_filter( 'post_row_actions', 'true_duplicate_post_link', 10, 2 );