<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<!-- Fixed navbar -->
	<nav id="header" class="navbar navbar-fixed-top">
		<div id="header-container" class="container navbar-container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a id="brand" class="navbar-brand" href="<?php echo site_url(); ?>">
					<img src="<?php bloginfo('template_directory');?>/img/logo.png" alt="">
				</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse pull-right-lg">
				<ul class="nav navbar-nav">
					<li class="active"><a href="<?php echo site_url(); ?>/calculator">Калькулятор</a></li>
					<li><a href="<?php echo site_url(); ?>/post-news">Новости</a></li>
					<li><a href="<?php echo site_url(); ?>/documents">Документация</a></li>
					<li><a href="<?php echo site_url(); ?>/#team">Команда</a></li>
					<li><a href="<?php echo site_url(); ?>/questions">Вопросы</a></li>
					<li><a href="<?php echo site_url(); ?>/#contact">Контакты</a></li>
					
					<?php if( is_user_logged_in() ){ ?>
						<li><a href="<?php echo site_url(); ?>/private-office/">Кабинет</a></li>
					<?php }else { ?>
						<li><a href="<?php echo site_url(); ?>/registration/">Регистрация</a></li>
						<li><a href="<?php echo site_url(); ?>/login/">Кабинет</a></li>
					<?php } ?>
					
					<!--
					<li>
						<div class="dropdown">
							<button class="dropdown-toggle" type="button" data-toggle="dropdown">
								<span class="locale">RU</span>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="">RU</a></li>
								<li><a href="">EN</a></li>
							</ul>
						</div>
					</li>-->
				</ul>
			</div><!-- /.nav-collapse -->
		</div><!-- /.container -->
	</nav><!-- /.navbar -->
	
	<!-- Section Club Top -->
	<section class="section-club-top section-news">
		<div class="container">
			<div class="row">
				<div class="col-md-7 col-sm-6 ">
					<h1>Новости</h1>
					<p>В данной категории можно ознакомится с событиям происходящими в индустрии деревообработки, зеленой энергетики и майнинга</p>
				</div>
			</div>
		</div>
	</section><!-- /.section-club-top -->
	
	<section class="section-content section-post">
		<div class="container">
			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">

				<?php if ( have_posts() ) : ?>

					<?php
					// Start the loop.
					while ( have_posts() ) : the_post();
					?>
					
						<article id="post-<?php the_ID(); ?>" <?php post_class('post-box small-post-box'); ?>>
							
							<div class="post-img small-post-img">	
								<a href="<?php the_permalink(); ?>">
									<?php
										if ( ! has_post_thumbnail() ) 
										{
									?>
											<img src="<?php bloginfo('template_directory');?>/img/default.png" class="img-thumbnail" width="250" height="150">
									<?php
										}
										else 
										{									
											the_post_thumbnail( 'post-thumbnail img-thumbnail', array( 'alt' => get_the_title() ) );
										}
									?>
									
									<div class="post-format"><i class="fa fa-file-text"></i></div>
								</a>
							</div>
							
							<div class="post-data small-post-data">
								<div class="post-data-container">
									<header class="entry-header">
										<?php
											the_title( sprintf( '<h2 class="entry-title post-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
										?>
									</header><!-- .entry-header -->
									
									<div class="entry-content post-excerpt">
										<?php the_content(); ?>
									</div><!-- .entry-content -->
									
									<div class="readmore">
										<a href="<?php the_permalink(); ?>">Читать далее</a>
									</div>
								</div>
							</div>

						</article><!-- #post-## -->
					
					<?php
					// End the loop.
					endwhile;
					
					wp_pagenavi();

				endif;
				?>

				</main><!-- .site-main -->
			</div><!-- .content-area -->
		</div>
	</section>
	
	<!-- Section Subscribe -->
	<section class="section-subscribe section-subscribe-news">
		<div class="container">
			<div class="row">
				<div class="subscription-container">

					<div class="col-md-6 col-sm-6 subscribe-description ">
						<div class="col-md-3 col-sm-12 clear-pads mail-img">
							<img src="<?php bloginfo('template_directory');?>/resources/images/mail.png">
						</div>
						<div class="col-md-9 col-sm-12">
							<h1><?php echo __( 'Подпишитесь', 'preico' ) ?></h1>
							<p><?php echo __( 'Еженедельная подпискa новостей.
								Информация, которая будет отсылаться
								при подписке.', 'preico' ) ?>
							</p>
						</div>

					</div>
					<div class="col-md-6 col-sm-6">
						<?php es_subbox( $namefield = "YES", $desc = "", $group = "" ); ?>

					</div>
				</div>
			</div>
		</div>
	</section><!-- /.section-subscribe -->

<?php get_footer(); ?>
