<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="format-detection" content="telephone=no">

	<title><?php echo get_bloginfo( 'name' ); ?></title>

	<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_directory');?>/img/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('template_directory');?>/img/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_directory');?>/img/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_directory');?>/img/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_directory');?>/img/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_directory');?>/img/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('template_directory');?>/img/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('template_directory');?>/img/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_directory');?>/img/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php bloginfo('template_directory');?>/img/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_directory');?>/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo('template_directory');?>/img/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_directory');?>/img/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php bloginfo('template_directory');?>/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php bloginfo('template_directory');?>/img/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	
	<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/style.css">	

	<?php wp_head();?>
	
	<script>
		var ajaxurl  = '<?php echo get_site_url(); ?>/wp-admin/admin-ajax.php';
	</script>
	
</head>
<body>

	<!-- Fixed navbar -->
	<nav id="header" class="navbar navbar-fixed-top">
		<div id="header-container" class="container navbar-container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a id="brand" class="navbar-brand" href="<?php echo site_url(); ?><?php echo wpml_site_link(); ?>">
					<img src="<?php bloginfo('template_directory');?>/img/logo.png" alt="">
				</a>
			</div>
			
			<div id="navbar" class="collapse navbar-collapse pull-right-lg">
				<ul class="nav navbar-nav">
					<?php
						$menu_locations = get_nav_menu_locations();
						$menu = $menu_locations[ 'primary' ];
						$menu = wp_get_nav_menu_items( $menu );
						
						$num = 0;
						
						foreach ( $menu as $item )
						{
							$current = ( $item->object_id == get_queried_object_id() ) ? 'menu-item-active' : '';
							
							if (is_user_logged_in() && (($num === 5) || ($num === 6)))
							{
								$num++;
								continue;
							}
							
							if (!is_user_logged_in() && ($num === 7))
							{
								$num++;
								continue;
							}
					?>
						<li class="<?php echo $current; ?>">
							<a href="<?php echo $item->url;?>"><?php echo $item->title;?></a>
						</li>
					<?php 					
							$num++;
						} 
						
						my_flag_only_language_switcher();
					?>
				</ul>
			</div><!-- /.nav-collapse -->
		</div><!-- /.container -->
	</nav><!-- /.navbar -->