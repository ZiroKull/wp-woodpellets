<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
	
	<!-- Section Club Top -->
	<section class="section-club-top section-news">
		<div class="container">
			<div class="row">
				<div class="col-md-7 col-sm-6 ">
					<h1><?php echo __( 'Новости', 'preico' ) ?></h1>
					<p><?php echo __( 'В данной категории можно ознакомится с событиям происходящими в индустрии деревообработки, зеленой энергетики и майнинга', 'preico' ) ?></p>
				</div>
			</div>
		</div>
	</section><!-- /.section-club-top -->
	
	<section class="section-content section-post">
		<div class="container">
			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">
				
				<?php
					$category = get_category( get_query_var( 'cat' ) );

					$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
					$query = new WP_Query( 
						array(
							'paged'         => $paged, 
							'category_name' => $category->slug,
							'order'         => 'asc',
							'post_type'     => 'post',
							'post_status'   => 'publish',
						)
					);
					if ($query->have_posts()) :
						while ($query->have_posts()) : 
							$query->the_post();
							
							global $post;
					?>
					
						<article id="post-<?php the_ID(); ?>" <?php post_class('post-box small-post-box'); ?>>
							
							<div class="post-img small-post-img">	
								<a href="<?php echo site_url(); ?>/<?php echo $category->slug; ?>/<?php the_ID(); ?>-<?php echo $post->post_name; ?>">
									<?php
										if ( ! has_post_thumbnail() ) 
										{
									?>
											<img src="<?php bloginfo('template_directory');?>/img/default.png" class="img-thumbnail" width="250" height="150">
									<?php
										}
										else 
										{									
											the_post_thumbnail( 'post-thumbnail img-thumbnail', array( 'alt' => get_the_title() ) );
										}
									?>
									
									<div class="post-format"><i class="fa fa-file-text"></i></div>
								</a>
							</div>
							
							<div class="post-data small-post-data">
								<div class="post-data-container">
									<header class="entry-header">
										<h2 class="entry-title post-title">
											<a href="<?php echo site_url(); ?>/<?php echo $category->slug; ?>/<?php the_ID(); ?>-<?php echo $post->post_name; ?>" rel="bookmark"><?php the_title( ); ?></a>
										</h2>									
									</header><!-- .entry-header -->
									
									<div class="entry-content post-excerpt">
										<?php the_content(); ?>
									</div><!-- .entry-content -->
									
									<div class="readmore">
										<a href="<?php echo site_url(); ?>/<?php echo $category->slug; ?>/<?php the_ID(); ?>-<?php echo $post->post_name; ?>"><?php echo __( 'Читать далее', 'preico' ) ?></a>
									</div>
								</div>
							</div>

						</article><!-- #post-## -->
					
					<?php
					// End the loop.
					endwhile;
					
					wp_pagenavi();
					
					wp_reset_postdata();

				endif;
				?>

				</main><!-- .site-main -->
			</div><!-- .content-area -->
		</div>
	</section>
	
	<!-- Section Subscribe -->
	<section class="section-subscribe section-subscribe-news">
		<div class="container">
			<div class="row">
				<div class="subscription-container">

					<div class="col-md-6 col-sm-6 subscribe-description ">
						<div class="col-md-3 col-sm-12 clear-pads mail-img">
							<img src="<?php bloginfo('template_directory');?>/resources/images/mail.png">
						</div>
						<div class="col-md-9 col-sm-12">
							<h1><?php echo __( 'Подпишитесь', 'preico' ) ?></h1>
							<p><?php echo __( 'Еженедельная подпискa новостей.
								Информация, которая будет отсылаться
								при подписке.', 'preico' ) ?>
							</p>
						</div>

					</div>
					<div class="col-md-6 col-sm-6">
						<?php es_subbox( $namefield = "YES", $desc = "", $group = "" ); ?>

					</div>
				</div>
			</div>
		</div>
	</section><!-- /.section-subscribe -->

<?php get_footer(); ?>
