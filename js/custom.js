jQuery(document).ready(function()
{
	/**
	 * This object controls the nav bar. Implement the add and remove
	 * action over the elements of the nav bar that we want to change.
	 *
	 * @type {{flagAdd: boolean, elements: string[], add: Function, remove: Function}}
	 */
	var myNavBar = {

		flagAdd: true,

		elements: [],

		init: function (elements) {
			this.elements = elements;
		},

		add : function() {
			if(this.flagAdd) {
				for(var i=0; i < this.elements.length; i++) {
					document.getElementById(this.elements[i]).className += " fixed-theme";
				}
				this.flagAdd = false;
			}
		},

		remove: function() {
			for(var i=0; i < this.elements.length; i++) {
				document.getElementById(this.elements[i]).className =
						document.getElementById(this.elements[i]).className.replace( /(?:^|\s)fixed-theme(?!\S)/g , '' );
			}
			this.flagAdd = true;
		}

	};

	/**
	 * Init the object. Pass the object the array of elements
	 * that we want to change when the scroll goes down
	 */
	myNavBar.init(  [
		"header",
		"header-container",
		"brand"
	]);

	/**
	 * Function that manage the direction
	 * of the scroll
	 */
	function offSetManager(){

		var yOffset = 0;
		var currYOffSet = window.pageYOffset;

		if(yOffset < currYOffSet) {
			myNavBar.add();
		}
		else if(currYOffSet == yOffset){
			myNavBar.remove();
		}

	}

	/**
	 * bind to the document scroll detection
	 */
	window.onscroll = function(e) {
		offSetManager();
	}

	/**
	 * We have to do a first detectation of offset because the page
	 * could be load with scroll down set.
	 */
	offSetManager();
	
	//new ClipboardJS('.copy');
	
	jQuery('a[href^="#"]').on('click',function (e) {
        e.preventDefault();

        var target = this.hash;
        var jQuerytarget = jQuery(target);
        var scroll;

        if(jQuery(window).scrollTop()==0){
            scroll =  (jQuerytarget.offset().top) - 160
        }else{
            scroll =  (jQuerytarget.offset().top) - 60
        }
        jQuery('html, body').stop().animate({
            'scrollTop': scroll
        }, 900, 'swing', function () {
            //window.location.hash = target;
        });
		
		if (jQuery(this).attr('href') != '#wpclub')
		{
			if(jQuery('.navbar-toggle').css('display') != 'none')
			{
				jQuery('.navbar-toggle').trigger( "click" );
			}
		}
    });
	
	scrollUp = jQuery('#scrollup');
    jQuery(window).scroll(function() 
	{
        if (jQuery(window).scrollTop() > jQuery(window).height() / 2 && scrollUp.not('.visible')) 
		{
            scrollUp.addClass('visible');
        } else if (scrollUp.is('.visible')) 
		{
            scrollUp.removeClass('visible');
        }
    });

	get_team_members = function ( more ) 
	{
		if(more)
		{
			jQuery('.team-member-block').slice(4).slideToggle( 400 );
			jQuery('.more-btn').removeClass('more')
		}
		else
		{
			jQuery('.team-member-block').slice(4).slideToggle( 400 );
			jQuery('.more-btn').addClass('more')
		}
	};

	jQuery(document).ready(function () 
	{
		jQuery(document).on('click', '.more-btn', function () {
			var more = jQuery(this).hasClass('more');
			get_team_members(more);
		});
	});
	
	var data = {
        'action': 'check_countdown'
    };
    jQuery.ajax({
        url: ajaxurl,
        data: data,
        type: 'GET',
        success: function (response) 
		{
            var date = null;
            if( response != '' )
			{
                var countDownDate = new Date(response.split("-").reverse().join("-")).getTime();

				// Update the count down every 1 second
				var x = setInterval(function() 
				{

					// Get todays date and time
					var now = new Date().getTime();

					// Time calculations for days, hours, minutes and seconds
					var d = Math.abs(countDownDate - now) / 1000;                           // delta
					d -= (3 * 3600);
					var r = {};                                                                // result
					var s = {                                                                  // structure
						year: 31536000,
						month: 2592000,
						week: 604800, // uncomment row to ignore
						day: 86400,   // feel free to add your own row
						hour: 3600,
						minute: 60,
						second: 1
					};

					Object.keys(s).forEach(function(key){
						r[key] = Math.floor(d / s[key]);
						d -= r[key] * s[key];
					});
					
					var seconds = r['second'];
					var minutes = r['minute'];
					var hours = r['hour'];
					var days = r['month'] * 30 + r['week'] * 7 + r['day'];

					if (hours   < 10) {hours   = "0"+hours;}
					if (minutes < 10) {minutes = "0"+minutes;}
					if (seconds < 10) {seconds = "0"+seconds;}

					if(  jQuery( '.counter-wrap .days .counter-number' ).text() != days && days >= 0 ){
						jQuery( '.counter-wrap .days .counter-number' ).text( days );
					}else{
						if( days >= 0 ){
							jQuery( '.counter-wrap .days .counter-number' ).text( days );
						}else{
							jQuery( '.counter-wrap .days .counter-number' ).text( 0 );
						}
					}


					if( jQuery( '.counter-wrap .hours .counter-number' ).text() != hours && hours >= 0 ){
						jQuery( '.counter-wrap .hours .counter-number' ).hide();
						jQuery( '.counter-wrap .hours .counter-number' ).text( hours );
						jQuery( '.counter-wrap .hours .counter-number' ).fadeIn( "slow");
					}else{
						if( hours >= 0 ){
							jQuery( '.counter-wrap .hours .counter-number' ).text( hours );
						}else{
							jQuery( '.counter-wrap .hours .counter-number' ).text( 0 );
						}
					}


					if( jQuery( '.counter-wrap .minutes .counter-number' ).text() != minutes && minutes >= 0 ){
						jQuery( '.counter-wrap .minutes .counter-number' ).hide();
						jQuery( '.counter-wrap .minutes .counter-number' ).text( minutes );
						jQuery( '.counter-wrap .minutes .counter-number' ).fadeIn( "slow");
					}else{
						if( minutes >= 0 ){
							jQuery( '.counter-wrap .minutes .counter-number' ).text( minutes );
						}else{
							jQuery( '.counter-wrap .minutes .counter-number' ).text( 0 );
						}
					}

					if( jQuery('.counter-wrap .seconds .counter-number').text() != seconds && seconds >= 0 ){
						jQuery( '.counter-wrap .seconds .counter-number' ).hide();
						jQuery( '.counter-wrap .seconds .counter-number' ).text( seconds );
						jQuery( '.counter-wrap .seconds .counter-number' ).fadeIn( "slow");
					}else{
						if( minutes >= 0 ){
							jQuery( '.counter-wrap .seconds .counter-number' ).text( seconds );
						}else{
							jQuery( '.counter-wrap .seconds .counter-number' ).text( 0 );
						}
					}

					// If the count down is finished, write some text
					if ((countDownDate - now) < 0) {
						clearInterval(x);
						//
					}
				}, 1000);
			}
		}
    });
	
	jQuery( document ).on( 'click', '.colapse-icon', function () {
        jQuery( '.side-menu' ).toggleClass( 'open' );
        jQuery( '.content-wrap' ).toggleClass( 'extended' );
    } );
	
	jQuery( document ).on( 'click', '.team-member-block img', function (e) {
		
        jQuery(e.target).closest('.team-member-block').find('#team-desc').toggleClass( 'hide', 'show');
        
    } );
	
	var settings = {
		slideWidth:300,
		minSlides:1,
		maxSlides: 5,
		moveSlides:1,
		speed:400,
		useCSS: false,
		pager:!1,
		pause:3000,
		infiniteLoop: true,
		autoControls: false,
		controls:!0,
		slideMargin: 40,
		touchEnabled: false,
		auto:!0
		//hideControlOnEnd: true
	};

	jQuery( '.partners-wrap' ).bxSlider( settings );
	
	jQuery.getJSON( domain + "/eth/ico.php?" + Math.random().toString(36).substr(2, 5), function( data )
	{
		var eth_eur     = data['eth_eur'];
		var balance_eth = data['balance_eth'];
		var updated     = data['updated'];

		var sum_eur = data['sum_eur'];

		var date = new Date(updated * 1000);
		var tm = ('0' + date.getDate()).slice(-2) + '.' + ('0' + (date.getMonth() + 1)).slice(-2) + '.' + date.getFullYear() + ' ' + ('0' + date.getHours()).slice(-2) + ':' + ('0' + date.getMinutes()).slice(-2);

		jQuery('.curently-recived span').text((+balance_eth).toFixed(3) + ' ETH');
		jQuery('.curently-recived-value').text(sum_eur.toFixed(0) + ' EUR на ' + tm +  ' MSK (GMT+3)');
		jQuery('.eth_txt').text((+eth_eur).toFixed(2) + ' EUR на ' + tm +  ' MSK (GMT+3)');
		
		var iCoPts = 0;
		var iCoLen = 0;
		
		if (+sum_eur <= 250000)
		{
			iCoPts = ((sum_eur * 100) / 250000) / 2;
			iCoLen = iCoPts;
		}
		else
		{
			var sum_eur1 = sum_eur - 250000;
			iCoLen = 50 + (((sum_eur1 * 100) / 750000) / 2);
		}
		
		jQuery('.circle-chart__circle').attr('stroke-dasharray', iCoLen + ',100');
	});

	jQuery('.calculate').click(function(e)
	{
		e.preventDefault();
		
		jQuery.getJSON( domain + "/eth/ico.php?" + Math.random().toString(36).substr(2, 5) , function( data )
		{
			var eth_eur     = data['eth_eur'];
			var balance_eth = data['balance_eth'];
			var updated     = data['updated'];

			var sum_eur = data['sum_eur'];

			var date = new Date(updated * 1000);
			var tm = ('0' + date.getDate()).slice(-2) + '.' + ('0' + (date.getMonth() + 1)).slice(-2) + '.' + date.getFullYear() + ' ' + ('0' + date.getHours()).slice(-2) + ':' + ('0' + date.getMinutes()).slice(-2);

			jQuery('.curently-recived span').text((+balance_eth).toFixed(3) + ' ETH');
			jQuery('.curently-recived-value').text(sum_eur.toFixed(0) + ' EUR на ' + tm +  ' MSK (GMT+3)');
			jQuery('.eth_txt').text((+eth_eur).toFixed(2) + ' EUR на ' + tm +  ' MSK (GMT+3)');
			jQuery('.must_pay').css('visibility', 'visible');

			var crat = 3;

			if (sum_eur < 250000)
			{
				crat = 4;
			}

			var all = (jQuery('#calc').val() / crat) / eth_eur;

			jQuery('.must_pay span').text( (all).toFixed(8) + ' ETH (' + (jQuery('#calc').val() / crat).toFixed(2) + ' EUR)'  );
			
			var iCoPts = 0;
			var iCoLen = 0;
			
			if (+sum_eur <= 250000)
			{
				iCoPts = ((sum_eur * 100) / 250000) / 2;
				iCoLen = iCoPts;
			}
			else
			{
				var sum_eur1 = sum_eur - 250000;
				iCoLen = 50 + (((sum_eur1 * 100) / 750000) / 2)
			}
			
			jQuery('.circle-chart__circle').attr('stroke-dasharray', iCoLen + ',100');
		});
		
		return false;
	});
});

function copyToClipboard(el) 
{
    // resolve the element
    el = (typeof el === 'string') ? document.querySelector(el) : el;

    // handle iOS as a special case
    if (navigator.userAgent.match(/ipad|ipod|iphone/i)) {

        // save current contentEditable/readOnly status
        var editable = el.contentEditable;
        var readOnly = el.readOnly;

        // convert to editable with readonly to stop iOS keyboard opening
        el.contentEditable = true;
        el.readOnly = true;

        // create a selectable range
        var range = document.createRange();
        range.selectNodeContents(el);

        // select the range
        var selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);
        el.setSelectionRange(0, 999999);

        // restore contentEditable/readOnly to original state
        el.contentEditable = editable;
        el.readOnly = readOnly;
    }
    else {
        el.select();
    }

    // execute copy command
    document.execCommand('copy');
}






