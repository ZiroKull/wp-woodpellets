jQuery(document).ready(function()
{
	/**
	 * This object controls the nav bar. Implement the add and remove
	 * action over the elements of the nav bar that we want to change.
	 *
	 * @type {{flagAdd: boolean, elements: string[], add: Function, remove: Function}}
	 */
	var myNavBar = {

		flagAdd: true,

		elements: [],

		init: function (elements) {
			this.elements = elements;
		},

		add : function() {
			if(this.flagAdd) {
				for(var i=0; i < this.elements.length; i++) {
					document.getElementById(this.elements[i]).className += " fixed-theme";
				}
				this.flagAdd = false;
			}
		},

		remove: function() {
			for(var i=0; i < this.elements.length; i++) {
				document.getElementById(this.elements[i]).className =
						document.getElementById(this.elements[i]).className.replace( /(?:^|\s)fixed-theme(?!\S)/g , '' );
			}
			this.flagAdd = true;
		}

	};

	/**
	 * Init the object. Pass the object the array of elements
	 * that we want to change when the scroll goes down
	 */
	myNavBar.init(  [
		"header",
		"header-container",
		"brand"
	]);

	/**
	 * Function that manage the direction
	 * of the scroll
	 */
	function offSetManager(){

		var yOffset = 0;
		var currYOffSet = window.pageYOffset;

		if(yOffset < currYOffSet) {
			myNavBar.add();
		}
		else if(currYOffSet == yOffset){
			myNavBar.remove();
		}

	}

	/**
	 * bind to the document scroll detection
	 */
	window.onscroll = function(e) {
		offSetManager();
	}

	/**
	 * We have to do a first detectation of offset because the page
	 * could be load with scroll down set.
	 */
	offSetManager();
	
	var eth_eur     = ''
	var balance_eth = '';
	var crat = 4;
	
	jQuery.getJSON( domain + "/eth/ico.php?" + Math.random().toString(36).substr(2, 5) , function( data )
	{
		console.log(data);

		eth_eur     = data['eth_eur'];
		balance_eth = data['balance_eth'];
		var updated     = data['updated'];

		var date = new Date(updated * 1000);
		var tm = ('0' + date.getDate()).slice(-2) + '.' + ('0' + (date.getMonth() + 1)).slice(-2) + '.' + date.getFullYear() + ' ' + ('0' + date.getHours()).slice(-2) + ':' + ('0' + date.getMinutes()).slice(-2);

		jQuery('.eth_txt').text((+eth_eur).toFixed(2) + ' EUR на ' + tm +  ' MSK (UTC+3)');

		crat = 4;
		var all = (100 * crat);

		jQuery('#totalInvest').text( (all).toFixed(0) );
	});
	
	jQuery(document).on('change input', '#sale', function()
	{
		var $val = jQuery(this).val();
		
		console.log( $val );
		
		jQuery('#saleValue').text( $val + '%' );
		
		if ($val === '75')
		{
			crat = 4;
			jQuery('#saleText').text( 'Soft Cap 0 - 250 000 €' );
		}
		else 
		{
			crat = 2;
			jQuery('#saleText').text( 'Hard Cap 250 000 - 1 000 000 €' );
		}
		
		var sum_eur = jQuery('#invests').val();
		
		var all = (sum_eur * crat);

		jQuery('#totalInvest').text( ((all).toFixed(0)).toLocaleString() );
		
		if (all < 500)
		{
			jQuery('.cacheback').css( 'display', 'none' );
			jQuery('.dividends').css( 'display', 'none' );
			jQuery('.maining').css( 'display', 'none' );
			
			jQuery('#optionHtml').text( (+all).toLocaleString() );
			jQuery('#total').text( (+all).toLocaleString() );	
			
			jQuery('.club-item').css( 'display', 'none' );
			jQuery('.club-item-begin').css( 'display', 'block' );
		}
		else if ((all >= 500) && (all < 5000))
		{
			jQuery('.cacheback').css( 'display', 'flex' );
			jQuery('.dividends').css( 'display', 'none' );
			jQuery('.maining').css( 'display', 'none' );
			
			var options   = (+all).toFixed(0);
			var cacheback = (+all * 0.07).toFixed(0);
			
			jQuery('#optionHtml').text( (+options).toLocaleString() );
			jQuery('#cachebackHtml').text( (+cacheback).toLocaleString() );
			jQuery('#total').text( (+options + +cacheback).toLocaleString() );

			jQuery('.club-item').css( 'display', 'none' );
			jQuery('.club-item-bronze').css( 'display', 'block' );
		}
		else if ((all >= 5000) && (all < 50000))
		{
			jQuery('.cacheback').css( 'display', 'flex' );
			jQuery('.dividends').css( 'display', 'flex' );
			jQuery('.maining').css( 'display', 'none' );
			
			var options   = (+all).toFixed(0);
			var cacheback = (+all * 0.07).toFixed(0);
			var dividends = (+all * 0.35).toFixed(0);
			
			jQuery('#optionHtml').text( (+options).toLocaleString() );
			jQuery('#cachebackHtml').text( (+cacheback).toLocaleString() );
			jQuery('#dividendsHtml').text( (+dividends).toLocaleString() );
			jQuery('#total').text( (+options + +cacheback + +dividends).toLocaleString() );
			
			jQuery('.club-item').css( 'display', 'none' );
			jQuery('.club-item-silver').css( 'display', 'block' );
		}
		else if ((all >= 50000) && (all < 500000))
		{
			jQuery('.cacheback').css( 'display', 'flex' );
			jQuery('.dividends').css( 'display', 'flex' );
			jQuery('.maining').css( 'display', 'flex' );
			
			var options   = (+all).toFixed(0);
			var cacheback = (+all * 0.07).toFixed(0);
			var dividends = (+all * 0.35).toFixed(0);
			var maining = (+all).toFixed(0);
			
			jQuery('#optionHtml').text( (+options).toLocaleString() );
			jQuery('#cachebackHtml').text( (+cacheback).toLocaleString() );
			jQuery('#dividendsHtml').text( (+dividends).toLocaleString() );
			jQuery('#mainingHtml').text( (+maining).toLocaleString() );
			jQuery('#total').text( (+options + +cacheback + +dividends + +maining).toLocaleString() );
			
			jQuery('.club-item').css( 'display', 'none' );
			jQuery('.club-item-gold').css( 'display', 'block' );
		}
		else if (all >= 500000)
		{
			jQuery('.cacheback').css( 'display', 'flex' );
			jQuery('.dividends').css( 'display', 'flex' );
			jQuery('.maining').css( 'display', 'flex' );

			var options   = (+all).toFixed(0);
			var cacheback = (+all * 0.07).toFixed(0);
			var dividends = (+all * 0.35).toFixed(0);
			var maining = (all).toFixed(0);
			
			jQuery('#optionHtml').text( (+options).toLocaleString() );
			jQuery('#cachebackHtml').text( (+cacheback).toLocaleString() );
			jQuery('#dividendsHtml').text( (+dividends).toLocaleString() );
			jQuery('#mainingHtml').text( (+maining).toLocaleString() );
			jQuery('#total').text( (+options + +cacheback + +dividends + +maining).toLocaleString() );
			
			jQuery('.club-item').css( 'display', 'none' );
			jQuery('.club-item-premium').css( 'display', 'block' );
		}
	});
	
	jQuery(document).on('change input', '#invests', function()
	{
		var $val = jQuery('#sale').val();
		
		if ($val === '75')
		{
			crat = 4;
		}
		else 
		{
			crat = 2;
		}
		
		var sum_eur = jQuery(this).val();
		
		var all = (sum_eur * crat);

		jQuery('#totalInvest').text( ((all).toFixed(0)).toLocaleString() );
		jQuery('#investInput').val( jQuery(this).val() );
		
		if (all < 500)
		{
			jQuery('.cacheback').css( 'display', 'none' );
			jQuery('.dividends').css( 'display', 'none' );
			jQuery('.maining').css( 'display', 'none' );
			
			jQuery('#optionHtml').text( (+all).toLocaleString() );
			jQuery('#total').text( (+all).toLocaleString() );	
			
			jQuery('.club-item').css( 'display', 'none' );
			jQuery('.club-item-begin').css( 'display', 'block' );
		}
		else if ((all >= 500) && (all < 5000))
		{
			jQuery('.cacheback').css( 'display', 'flex' );
			jQuery('.dividends').css( 'display', 'none' );
			jQuery('.maining').css( 'display', 'none' );
			
			var options   = (+all).toFixed(0);
			var cacheback = (+all * 0.07).toFixed(0);
			
			jQuery('#optionHtml').text( (+options).toLocaleString() );
			jQuery('#cachebackHtml').text( (+cacheback).toLocaleString() );
			jQuery('#total').text( (+options + +cacheback).toLocaleString() );

			jQuery('.club-item').css( 'display', 'none' );
			jQuery('.club-item-bronze').css( 'display', 'block' );
		}
		else if ((all >= 5000) && (all < 50000))
		{
			jQuery('.cacheback').css( 'display', 'flex' );
			jQuery('.dividends').css( 'display', 'flex' );
			jQuery('.maining').css( 'display', 'none' );
			
			var options   = (+all).toFixed(0);
			var cacheback = (+all * 0.07).toFixed(0);
			var dividends = (+all * 0.35).toFixed(0);
			
			jQuery('#optionHtml').text( (+options).toLocaleString() );
			jQuery('#cachebackHtml').text( (+cacheback).toLocaleString() );
			jQuery('#dividendsHtml').text( (+dividends).toLocaleString() );
			jQuery('#total').text( (+options + +cacheback + +dividends).toLocaleString() );
			
			jQuery('.club-item').css( 'display', 'none' );
			jQuery('.club-item-silver').css( 'display', 'block' );
		}
		else if ((all >= 50000) && (all < 500000))
		{
			jQuery('.cacheback').css( 'display', 'flex' );
			jQuery('.dividends').css( 'display', 'flex' );
			jQuery('.maining').css( 'display', 'flex' );
			
			var options   = (+all).toFixed(0);
			var cacheback = (+all * 0.07).toFixed(0);
			var dividends = (+all * 0.35).toFixed(0);
			var maining = (+all).toFixed(0);
			
			jQuery('#optionHtml').text( (+options).toLocaleString() );
			jQuery('#cachebackHtml').text( (+cacheback).toLocaleString() );
			jQuery('#dividendsHtml').text( (+dividends).toLocaleString() );
			jQuery('#mainingHtml').text( (+maining).toLocaleString() );
			jQuery('#total').text( (+options + +cacheback + +dividends + +maining).toLocaleString() );
			
			jQuery('.club-item').css( 'display', 'none' );
			jQuery('.club-item-gold').css( 'display', 'block' );
		}
		else if (all >= 500000)
		{
			jQuery('.cacheback').css( 'display', 'flex' );
			jQuery('.dividends').css( 'display', 'flex' );
			jQuery('.maining').css( 'display', 'flex' );

			var options   = (+all).toFixed(0);
			var cacheback = (+all * 0.07).toFixed(0);
			var dividends = (+all * 0.35).toFixed(0);
			var maining = (all).toFixed(0);
			
			jQuery('#optionHtml').text( (+options).toLocaleString() );
			jQuery('#cachebackHtml').text( (+cacheback).toLocaleString() );
			jQuery('#dividendsHtml').text( (+dividends).toLocaleString() );
			jQuery('#mainingHtml').text( (+maining).toLocaleString() );
			jQuery('#total').text( (+options + +cacheback + +dividends + +maining).toLocaleString() );
			
			jQuery('.club-item').css( 'display', 'none' );
			jQuery('.club-item-premium').css( 'display', 'block' );
		}
	});
	
	jQuery(document).on('change input', '#investInput', function()
	{
		var $val = jQuery('#sale').val();
		
		if ($val === '75')
		{
			crat = 4;
		}
		else 
		{
			crat = 2;
		}
		
		var sum_eur = jQuery(this).val();
		
		if (sum_eur < 1)
		{
			sum_eur = 1;
		}
		else if (sum_eur > 250000)
		{
			jQuery(this).val(250000);
			sum_eur = 250000;
		}
		
		jQuery('#invests').val( sum_eur );
		
		
		var all = (sum_eur * crat);

		jQuery('#totalInvest').text( ((all).toFixed(0)).toLocaleString() );
		jQuery('#investInput').val( jQuery(this).val() );
		
		if (all < 500)
		{
			jQuery('.cacheback').css( 'display', 'none' );
			jQuery('.dividends').css( 'display', 'none' );
			jQuery('.maining').css( 'display', 'none' );
			
			jQuery('#optionHtml').text( (+all).toLocaleString() );
			jQuery('#total').text( (+all).toLocaleString() );	
			
			jQuery('.club-item').css( 'display', 'none' );
			jQuery('.club-item-begin').css( 'display', 'block' );
		}
		else if ((all >= 500) && (all < 5000))
		{
			jQuery('.cacheback').css( 'display', 'flex' );
			jQuery('.dividends').css( 'display', 'none' );
			jQuery('.maining').css( 'display', 'none' );
			
			var options   = (+all).toFixed(0);
			var cacheback = (+all * 0.07).toFixed(0);
			
			jQuery('#optionHtml').text( (+options).toLocaleString() );
			jQuery('#cachebackHtml').text( (+cacheback).toLocaleString() );
			jQuery('#total').text( (+options + +cacheback).toLocaleString() );

			jQuery('.club-item').css( 'display', 'none' );
			jQuery('.club-item-bronze').css( 'display', 'block' );
		}
		else if ((all >= 5000) && (all < 50000))
		{
			jQuery('.cacheback').css( 'display', 'flex' );
			jQuery('.dividends').css( 'display', 'flex' );
			jQuery('.maining').css( 'display', 'none' );
			
			var options   = (+all).toFixed(0);
			var cacheback = (+all * 0.07).toFixed(0);
			var dividends = (+all * 0.35).toFixed(0);
			
			jQuery('#optionHtml').text( (+options).toLocaleString() );
			jQuery('#cachebackHtml').text( (+cacheback).toLocaleString() );
			jQuery('#dividendsHtml').text( (+dividends).toLocaleString() );
			jQuery('#total').text( (+options + +cacheback + +dividends).toLocaleString() );
			
			jQuery('.club-item').css( 'display', 'none' );
			jQuery('.club-item-silver').css( 'display', 'block' );
		}
		else if ((all >= 50000) && (all < 500000))
		{
			jQuery('.cacheback').css( 'display', 'flex' );
			jQuery('.dividends').css( 'display', 'flex' );
			jQuery('.maining').css( 'display', 'flex' );
			
			var options   = (+all).toFixed(0);
			var cacheback = (+all * 0.07).toFixed(0);
			var dividends = (+all * 0.35).toFixed(0);
			var maining = (+all).toFixed(0);
			
			jQuery('#optionHtml').text( (+options).toLocaleString() );
			jQuery('#cachebackHtml').text( (+cacheback).toLocaleString() );
			jQuery('#dividendsHtml').text( (+dividends).toLocaleString() );
			jQuery('#mainingHtml').text( (+maining).toLocaleString() );
			jQuery('#total').text( (+options + +cacheback + +dividends + +maining).toLocaleString() );
			
			jQuery('.club-item').css( 'display', 'none' );
			jQuery('.club-item-gold').css( 'display', 'block' );
		}
		else if (all >= 500000)
		{
			jQuery('.cacheback').css( 'display', 'flex' );
			jQuery('.dividends').css( 'display', 'flex' );
			jQuery('.maining').css( 'display', 'flex' );

			var options   = (+all).toFixed(0);
			var cacheback = (+all * 0.07).toFixed(0);
			var dividends = (+all * 0.35).toFixed(0);
			var maining = (all).toFixed(0);
			
			jQuery('#optionHtml').text( (+options).toLocaleString() );
			jQuery('#cachebackHtml').text( (+cacheback).toLocaleString() );
			jQuery('#dividendsHtml').text( (+dividends).toLocaleString() );
			jQuery('#mainingHtml').text( (+maining).toLocaleString() );
			jQuery('#total').text( (+options + +cacheback + +dividends + +maining).toLocaleString() );
			
			jQuery('.club-item').css( 'display', 'none' );
			jQuery('.club-item-premium').css( 'display', 'block' );
		}
	});
	
});
