jQuery(document).ready(function()
{	
	jQuery( document ).on( 'click', '.colapse-icon', function () {
        jQuery( '.side-menu' ).toggleClass( 'open' );
        jQuery( '.content-wrap' ).toggleClass( 'extended' );
    } );
	
	jQuery('#userUpdateForm').formValidation({
        message: 'This value is not valid',
		icon: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
        fields: {
            wpcrl_fname: {
                validators: {
                    notEmpty: {
                        message: 'The first name is required'
                    },
                    stringLength: {
                        max: 30,
                        message: 'The firstname must be less than 30 characters long'
                    }
                    //,
                    //regexp: {
                    //    regexp: /^[a-zA-Z]*jQuery/,
                    //    message: 'Only characters are allowed.'
                    //}
                }
            },
            wpcrl_lname:{
                validators: {
                    notEmpty: {
                        message: 'The first name is required'
                    },
                    stringLength: {
                        max: 30,
                        message: 'The firstname must be less than 30 characters long'
                    }
                    //,
                    //regexp: {
                    //    regexp: /^[a-zA-Z]*jQuery/,
                    //    message: 'Only characters are allowed.'
                    //}
                }
            },
            wpcrl_username: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The username is required'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'The username must be more than 6 and less than 30 characters long'
                    }
                    ,
                    regexp: {
                        regexp: /^[a-zA-Z0-9_\.]+$/,
                        message: 'The username can only consist of alphabetical, number, dot and underscore'
                    }
                }
            },
            wpcrl_email: {
               validators: {
                        notEmpty: {
                            message: 'The email is required'
                        },
                        regexp: {
                            regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                            message: 'The value is not a valid email address'
                        }
                    }
            },
            wpcrl_old_password: {
                validators: {
                    //notEmpty: {
                    //    message: 'The password is required'
                    //},
                    stringLength: {
                        min: 6,
                        message: 'The old password must be more than 6 characters long'
                    }
                }
            },
            wpcrl_new_password: {
                validators: {
                    //notEmpty: {
                    //    message: 'The password is required'
                    //},
                    stringLength: {
                        min: 6,
                        message: 'The new password must be more than 6 characters long'
                    }
                }
            },
            wpcrl_wallet: {
                validators: {
                    //notEmpty: {
                    //    message: 'The password is required'
                    //},
                    stringLength: {
                        min: 42,
						max: 42,
                        message: 'The Wallet must be only 42 characters long'
                    }
                }
            },
        }
    }).on('success.form.fv', function(e) {
        jQuery('#wpcrl-register-alert').hide();
        jQuery('#wpcrl-mail-alert').hide();
        jQuery('body, html').animate({
            scrollTop: 0
        }, 'slow');
        // You can get the form instance
        var jQueryregisterForm = jQuery(e.target);
        // and the FormValidation instance
        var fv = jQueryregisterForm.data('formValidation');
        var content = jQueryregisterForm.serialize();
        // start processing
        //jQuery('#wpcrl-reg-loader-info').show();

        updateUser(content);

        //wpcrlStartRegistrationProcess(content);
        // Prevent form submission
        e.preventDefault();
    }).on('err.form.fv', function(e) {
        // Regenerate the captcha
        //generateCaptcha();
    });
	
	function updateUser(content) 
	{
		jQuery.ajax({
			url: ajaxurl,
			data: content + '&action=update_user',
			type: 'POST',
			success: function (response) {
				
				jQuery('#user_details').modal('toggle');
			}
		});
	}
});