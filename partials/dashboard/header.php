<div class="col-md-12 col-sm-12 col-xs-12 dashboard-header yellow-bg">
	<div class="balance">
		<div class="text"><?php echo  __( 'Ваш баланс', 'preico' );?></div>
		<div class="summ"><?php echo getTokenBalance();  ?>
        <span>
            <img src="<?php bloginfo('template_directory');?>/resources/images/vikup_tokena.png">
        </span>
        </div>

	</div>

	<!--<button class="btn- btn-success invest">Инвестировать</button>-->
	
	<ul class="nav navbar-nav dashboard-menu">
		<li>
			<a href="" data-toggle="modal" data-target="#user_details" >
				<div class="menu-img-wrap">
					<img src="<?php bloginfo('template_directory');?>/resources/images/dropdown/personal_cabinet.png">
				</div>

				<div class="hver"><?php echo  __( 'личные данные инвестора', 'preico' );?></div>
			</a>
		</li>

		<li>
			<a class="" href="<?php echo wp_logout_url( site_url() ); ?>">
				<div class="menu-img-wrap">
					<img src="<?php bloginfo('template_directory');?>/resources/images/dropdown/exit.png">
				</div>

				<div class="hver"><?php echo __( 'Выйти', 'preico' ) ?></div>
			</a> 
		</li>
		
		<?php my_flag_only_language_switcher(); ?>
	</ul>

	<div class="dropdown user-info">
		<button class="dropdown-toggle" type="button" data-toggle="dropdown">
			<span class="locale"><?php echo wp_get_current_user()->user_firstname . ' ' . wp_get_current_user()->user_lastname ; ?></span>
		</button>
	</div>

</div>


