<?php /*var_dump( wp_get_current_user() )*/ ?>
<div class="modal fade" id="user_details" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body"><button type="button" class="close" data-dismiss="modal">&times;</button>

                <form name="userUpdateForm" id="userUpdateForm" method="post">
				
                    <div class="form-group">
                        <input type="text" class="form-control" name="wpcrl_fname" id="wpcrl_fname" placeholder="<?php echo __( 'Имя', 'preico' ) ?>" value="<?php echo wp_get_current_user()->user_firstname; ?>">
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" name="wpcrl_lname" id="wpcrl_lname" placeholder="<?php echo __( 'Фамилия', 'preico' ) ?>" value="<?php echo wp_get_current_user()->user_lastname ; ?>">
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" name="wpcrl_email" id="wpcrl_email" placeholder="<?php echo __( 'Email', 'preico' ) ?>" value="<?php echo wp_get_current_user()->user_email; ?>">
                    </div>
                    <div class="form-group pass-confirm">
                        <input type="password" class="form-control" name="wpcrl_new_password" id="wpcrl_new_password" placeholder="<?php echo __( 'Новый пароль', 'preico' ) ?>" >
                    </div>

					<div class="form-group pass-confirm">
                        <input type="text" class="form-control" name="wpcrl_wallet" id="wpcrl_wallet" placeholder="<?php echo __( 'Номер ETH кошелька', 'preico' ) ?>" value="<?php echo get_user_meta( get_current_user_id(), 'wpcrl_wallet', true );  ?>">
                    </div>

					<div class="form-group">
						<button type="submit" class="update btn"><?php echo __( 'Сохранить', 'preico' ) ?></button>
					</div>

                </form>

            </div>
        </div>

    </div>
</div>