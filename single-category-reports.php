<?php get_header('old'); ?>

    <div class="col-md-12 col-sm-12 col-xs-12 clear-pads dashboard-template">

        <?php get_template_part('partials/dashboard/side-menu');?>

        <div class="content-wrap col-md-12 col-sm-12 col-xs-12 extended">

            <?php get_template_part('partials/dashboard/header');?>

            <div class="col-md-12 col-sm-12 col-xs-12 dashboard dashboard-club">
			
				<div class="container">
				
					<div style="overflow: hidden;">
						<div class="readmore margin-t-20">
							<a href="<?php echo site_url(); ?><?php echo wpml_site_link(); ?>reports/"><?php echo __( 'Назад к Новостям', 'preico' ) ?></a>
						</div>
					</div>
			
					<?php
					// Start the loop.
					while ( have_posts() ) : the_post();

						/*
						 * Include the post format-specific template for the content. If you want to
						 * use this in a child theme, then include a file called content-___.php
						 * (where ___ is the post format) and that will be used instead.
						 */
						get_template_part( 'content', get_post_format() );

					// End the loop.
					endwhile;
					?>
					
					<div style="overflow: hidden;">
						<div class="readmore margin-t-20">
							<a href="<?php echo site_url(); ?><?php echo wpml_site_link(); ?>reports/"><?php echo __( 'Назад к Новостям', 'preico' ) ?></a>
						</div>
					</div>
				</div>
            </div>
        </div>

    </div>

    <div class="dashboard-modals">
        <?php get_template_part('partials/dashboard/user-edit-form');?>
    </div>

	<!-- jQuery first, then Tether, then Bootstrap JS. -->
	<script src="<?php bloginfo('template_directory');?>/js/jquery-1.11.1.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/bootstrap.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/validator/formValidation.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/validator/bootstrap-validator.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/dashboard.js"></script>