<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<!-- Fixed navbar -->
	<nav id="header" class="navbar navbar-fixed-top">
		<div id="header-container" class="container navbar-container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a id="brand" class="navbar-brand" href="<?php echo site_url(); ?>">
					<img src="<?php bloginfo('template_directory');?>/img/logo.png" alt="">
				</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse pull-right-lg">
				<ul class="nav navbar-nav">
					<li class="active"><a href="<?php echo site_url(); ?>/calculator">Калькулятор</a></li>
					<li><a href="<?php echo site_url(); ?>/post-news">Новости</a></li>
					<li><a href="<?php echo site_url(); ?>/documents">Документация</a></li>
					<li><a href="<?php echo site_url(); ?>/#team">Команда</a></li>
					<li><a href="<?php echo site_url(); ?>/questions">Вопросы</a></li>
					<li><a href="<?php echo site_url(); ?>/#contact">Контакты</a></li>
					
					<?php if( is_user_logged_in() ){ ?>
						<li><a href="<?php echo site_url(); ?>/private-office/">Кабинет</a></li>
					<?php }else { ?>
						<li><a href="<?php echo site_url(); ?>/registration/">Регистрация</a></li>
						<li><a href="<?php echo site_url(); ?>/login/">Кабинет</a></li>
					<?php } ?>
					
					<!--
					<li>
						<div class="dropdown">
							<button class="dropdown-toggle" type="button" data-toggle="dropdown">
								<span class="locale">RU</span>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="">RU</a></li>
								<li><a href="">EN</a></li>
							</ul>
						</div>
					</li>-->
				</ul>
			</div><!-- /.nav-collapse -->
		</div><!-- /.container -->
	</nav><!-- /.navbar -->
	
	<!-- Section Club Top -->
	<section class="section-club-top">
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-sm-6 ">
					<h1>Клуб WPC</h1>
					<p>Участниками клуба становятся покупатели и держатели токенов WPC</p>
				</div>
			</div>
		</div>
	</section><!-- /.section-club-top -->

	<section class="section-content section-post">
		<div class="container">
			<div class="row club-row-top">
				<div class="col-md-12">
					<div id="primary" class="content-area">
						<main id="main" class="site-main" role="main">

						<?php
						// Start the loop.
						while ( have_posts() ) : the_post();

							/*
							 * Include the post format-specific template for the content. If you want to
							 * use this in a child theme, then include a file called content-___.php
							 * (where ___ is the post format) and that will be used instead.
							 */
							get_template_part( 'content', get_post_format() );

						// End the loop.
						endwhile;
						?>
						
							<div class="readmore margin-t-20">
								<a href="<?php echo site_url(); ?>/post-news/">Назад к Новостям</a>
							</div>

						</main><!-- .site-main -->
					</div><!-- .content-area -->
				</div>
				
			</div>
		</div>
	</section>

<?php get_footer(); ?>
