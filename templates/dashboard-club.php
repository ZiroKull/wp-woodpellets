<?php
/**
 * Template Name: Dashboard Club
 */

get_header('old'); ?>



    <div class="col-md-12 col-sm-12 col-xs-12 clear-pads dashboard-template">

        <?php get_template_part('partials/dashboard/side-menu');?>

        <div class="content-wrap col-md-12 col-sm-12 col-xs-12 extended">

            <?php get_template_part('partials/dashboard/header');?>

            <div class="col-md-12 col-sm-12 col-xs-12 dashboard dashboard-club">
			
				<div class="container">
			
					<h3><b><?php echo  __( 'Клуб WPC', 'preico' );?></b></h3>
					
					<div class="row bottom-b-silver club-row-top">
					
						<?php
							$args = array(
								'post_type'      => 'club',
								'post_status'    => 'publish',
								'orderby'        => 'post_date',
								'order'          => 'ASC',
								'posts_per_page' => '2'
							);
							$posts = new WP_Query( $args );

							if ( $posts->have_posts() ) : ?>
					
								<?php while ( $posts->have_posts() ) : $posts->the_post(); ?>
								
									<?php $image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() )); ?>
									
									<div class="col-md-6 col-sm-6 club-item">
										<div class="club-left">
											<img src="<?php echo $image; ?>">
											
											<p class="bronze"><?php the_field('token-value'); ?></p>						
										</div>
										
										<div class="club-right">
											<h3><?php the_title(); ?></h3> 
											
											<p><?php the_content(); ?></p>
											<p class="yellow"><?php the_field('bonus'); ?></p>
										</div>
									</div>

								<?php 
									endwhile; 
								?>
							<?php wp_reset_query(); ?>
						<?php endif; ?>
					
					</div>
					
					<div class="row club-row-bottom">
						
						<?php
							$args = array(
								'post_type'      => 'club',
								'post_status'    => 'publish',
								'orderby'        => 'post_date',
								'order'          => 'ASC',
								'posts_per_page' => '2',
								'offset'         => '2'
							);
							$posts = new WP_Query( $args );

							if ( $posts->have_posts() ) : ?>
					
								<?php while ( $posts->have_posts() ) : $posts->the_post(); ?>
								
									<?php $image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() )); ?>
									
									<div class="col-md-6 col-sm-6 club-item">
										<div class="club-left">
											<img src="<?php echo $image; ?>">
											
											<p class="bronze"><?php the_field('token-value'); ?></p>						
										</div>
										
										<div class="club-right">
											<h3><?php the_title(); ?></h3> 
											
											<p><?php the_content(); ?></p>
											<p class="yellow"><?php the_field('bonus'); ?></p>
										</div>
									</div>

								<?php 
									endwhile; 
								?>
							<?php wp_reset_query(); ?>
						<?php endif; ?>
						
					</div>
					
					<div class="row bg-blue-gray">
						<div class="col-md-12">
							<p><?php echo  __( 'Начисление прибыли полностью автоматизированы и гарантируются самим смарт контрактом токена wpc.Выплаты будут производиться в криптовалюте Etherium (ETH) на кошелек держателя, в соответствии его клубного статуса по текущему курсу eth к евро за вычетом оплаты транзакции из кабинета инвестора.', 'preico' );?></p>
						</div>
					</div>
				</div>
            </div>
        </div>





    </div>

    <div class="dashboard-modals">
        <?php get_template_part('partials/dashboard/user-edit-form');?>
    </div>

	<!-- jQuery first, then Tether, then Bootstrap JS. -->
	<script src="<?php bloginfo('template_directory');?>/js/jquery-1.11.1.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/bootstrap.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/validator/formValidation.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/validator/bootstrap-validator.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/dashboard.js"></script>