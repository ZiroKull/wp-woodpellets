<?php
/**
 * Template Name: Dashboard
 */

get_header('old'); ?>
<?php $page = get_page_by_title( 'Dashboard' );
$content = apply_filters('the_content', $page->post_content); ?>

	<div class="col-md-12 col-sm-12 col-xs-12 clear-pads dashboard-template">

        <?php get_template_part('partials/dashboard/side-menu');?>

        <div class="content-wrap col-md-12 col-sm-12 col-xs-12 extended">


            <?php get_template_part('partials/dashboard/header');?>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-12">
                    <h4><?php echo __( 'Личный кабинет', 'preico' ) ?></h4>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 rounded">
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="yellow-bg">
                        <div class="description">
                            <p class="info-title"><?php echo __( 'Ваш баланс', 'preico' ) ?></p>
                            <p class="info"><?php echo getTokenBalance();  ?> </p>
                        </div>
                    </div>

                </div>

                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="gray-bg">
                        <div class="description">
                            <p class="info-title"><?php echo __( 'Статус', 'preico' ) ?></p>
                            <p class="info"><?php  
								$wpcrl_user_status = get_user_meta( get_current_user_id(), 'wpcrl_user_status', true );
								
								if ($wpcrl_user_status == 2)
								{
									echo __( 'Бронзовый', 'preico' );
								}
								else if ($wpcrl_user_status == 3)
								{
									echo __( 'Серебряный', 'preico' );
								}
								else if ($wpcrl_user_status == 4)
								{
									echo __( 'Золотой', 'preico' );
								}
								else if ($wpcrl_user_status == 5)
								{
									echo __( 'Платиновый', 'preico' );
								}
								else 
								{
									echo __( 'Начальный', 'preico' );
								}
							?></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12"><div class="col-md-12 col-sm-12 col-xs-12"><div class="col-md-12 col-sm-12 col-xs-12 border-bottom"></div></div></div>

            <div class="col-md-12 col-sm-12 col-xs-12 dashboard">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <?php echo $content; ?>
                </div>

                
                <div class="col-md-6 col-sm-12 col-xs-12" style="">
                    <a href="" class="" data-toggle="modal" data-target="#videoModal">
                        <div class="col-md-12 col-sm-12 col-xs-12 clear-pads video-preview" style="background: url(<?php echo $preview; ?>)no-repeat center center;
                            -webkit-background-size: cover;
                            -moz-background-size: cover;
                            -o-background-size: cover;
                            background-size: cover;">

                        </div>
                    </a>


                </div>
            </div>
        </div>

	</div>

    <div class="dashboard-modals">
        <?php get_template_part('partials/dashboard/user-edit-form');?>
    </div>

	<!-- jQuery first, then Tether, then Bootstrap JS. -->
	<script src="<?php bloginfo('template_directory');?>/js/jquery-1.11.1.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/bootstrap.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/validator/formValidation.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/validator/bootstrap-validator.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/dashboard.js"></script>
