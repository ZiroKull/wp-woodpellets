<?php
/**
 * Template Name: Dashboard CCTV
 */

get_header('old'); ?>

    <div class="col-md-12 col-sm-12 col-xs-12 clear-pads dashboard-template">

        <?php get_template_part('partials/dashboard/side-menu');?>

        <div class="content-wrap col-md-12 col-sm-12 col-xs-12 extended">

            <?php get_template_part('partials/dashboard/header');?>

            <div class="col-md-7 col-sm-12 col-xs-12 dashboard">
                <h3><b><?php echo  __( 'Дата-центр для майнинга', 'preico' );?></b></h3>
				
				<img src="<?php bloginfo('template_directory');?>/img/cctv/wd1.png" width="100%" class="padding-b-15">
				
				<h3><b><?php echo  __( 'Описания местоположения и процессов в конкретной локации', 'preico' );?>.</b></h3>
				<p><?php echo  __( 'Актуальная информация по лакации, также новости по улучшениям', 'preico' );?>.</p>
            </div>
			
			<div class="col-md-5 col-sm-12 col-xs-12 dashboard overflow">
				<h3><b><?php echo  __( 'Похожие', 'preico' );?></b></h3>
				<div class="col-md-12 col-sm-12 col-xs-12 clear-pads">
					<div class="col-md-12 col-sm-6 col-xs-12 clear-pads padding-b-15">
						<div class="col-md-5 col-sm-12 col-xs-12 clear-pads">
							<img src="<?php bloginfo('template_directory');?>/img/cctv/wd2.png">
						</div>
					
						<div class="col-md-7 col-sm-12 col-xs-12 clear-pads">
							<h3><b><?php echo  __( 'Когенерационная станция', 'preico' );?></b></h3>
							<p><?php echo  __( 'Описание', 'preico' );?></p>
						</div>
					</div>
					
					<div class="col-md-12 col-sm-6 col-xs-12 clear-pads padding-b-15">
						<div class="col-md-5 col-sm-12 col-xs-12 clear-pads">
							<img src="<?php bloginfo('template_directory');?>/img/cctv/wd3.png">
						</div>
					
						<div class="col-md-7 col-sm-12 col-xs-12 clear-pads">
							<h3><b><?php echo  __( 'Тепличный комплекс', 'preico' );?></b></h3>
							<p><?php echo  __( 'Описание', 'preico' );?></p>
						</div>
					</div>
				</div>
				
				<div class="col-md-12 col-sm-12 col-xs-12 clear-pads">
					<div class="col-md-12 col-sm-6 col-xs-12 clear-pads padding-b-15">
						<div class="col-md-5 col-sm-12 col-xs-12 clear-pads">
							<img src="<?php bloginfo('template_directory');?>/img/cctv/wd4.png">
						</div>
					
						<div class="col-md-7 col-sm-12 col-xs-12 clear-pads">
							<h3><b><?php echo  __( 'Лесные ресурсы', 'preico' );?></b></h3>
							<p><?php echo  __( 'Описание', 'preico' );?></p>
						</div>
					</div>
					
					<div class="col-md-12 col-sm-6 col-xs-12 clear-pads padding-b-15">
						<div class="col-md-5 col-sm-12 col-xs-12 clear-pads">
							<img src="<?php bloginfo('template_directory');?>/img/cctv/wd5.png">
						</div>
					
						<div class="col-md-7 col-sm-12 col-xs-12 clear-pads">
							<h3><b><?php echo  __( 'Лесозавод', 'preico' );?></b></h3>
							<p><?php echo  __( 'Описание', 'preico' );?></p>
						</div>
					</div>
				</div>
				
				<div class="col-md-12 col-sm-12 col-xs-12 clear-pads">
					<div class="col-md-12 col-sm-6 col-xs-12 clear-pads padding-b-15">
						<div class="col-md-5 col-sm-12 col-xs-12 clear-pads">
							<img src="<?php bloginfo('template_directory');?>/img/cctv/wd6.png">
						</div>
					
						<div class="col-md-7 col-sm-12 col-xs-12 clear-pads">
							<h3><b><?php echo  __( 'Завод древесных пеллет', 'preico' );?></b></h3>
							<p><?php echo  __( 'Описание', 'preico' );?></p>
						</div>
					</div>
				</div>
            </div>
        </div>

    </div>

    <div class="dashboard-modals">
        <?php get_template_part('partials/dashboard/user-edit-form');?>
    </div>

	<!-- jQuery first, then Tether, then Bootstrap JS. -->
	<script src="<?php bloginfo('template_directory');?>/js/jquery-1.11.1.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/bootstrap.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/validator/formValidation.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/validator/bootstrap-validator.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/dashboard.js"></script>