<?php
/**
 * Template Name: Default dashboard template
 */

get_header('old'); ?>



    <div class="col-md-12 col-sm-12 col-xs-12 clear-pads dashboard-template">

        <?php get_template_part('partials/dashboard/side-menu');?>

        <div class="content-wrap col-md-12 col-sm-12 col-xs-12 extended">


            <?php get_template_part('partials/dashboard/header');?>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-12">
                    <h4></h4>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-12 col-sm-12 col-xs-12"></div>
				</div>
			</div>

            <div class="col-md-12 col-sm-12 col-xs-12 dashboard">
			
                <div class="col-md-6 col-sm-12 col-xs-12">
					<h3><b><?php echo __( 'Данный раздел еще в разработке', 'preico' ) ?></b></h3>
                </div>

            </div>
        </div>





    </div>

    <div class="dashboard-modals">
        <?php get_template_part('partials/dashboard/user-edit-form');?>
    </div>

	<!-- jQuery first, then Tether, then Bootstrap JS. -->
	<script src="<?php bloginfo('template_directory');?>/js/jquery-1.11.1.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/bootstrap.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/validator/formValidation.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/validator/bootstrap-validator.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/dashboard.js"></script>