<?php
/**
 * Template Name: Documents
 */
get_header(); ?>

	<section class="document-template">
		<div id="banner">

			<div class="container document-container">

				<div class="col-md-4 col-sm-4 col-xs-12 right-md">
					<div class="flying-token-wrap">
						<img class="token2" src="<?php bloginfo('template_directory');?>/resources/images/token2.png">

					</div>
				</div>
				
				<div class="col-md-8 col-sm-8 col-xs-12">
					<p class="yellow-text"><?php echo  __( 'PRE ICO', 'preico' );?></p>
					<h1><?php echo  __( 'Проект Wood Pellets', 'preico' );?></h1>
					<p class="ico-desc"><?php echo  __( 'БЛОКЧЕЙН - ОПЦИОН В ИНДУСТРИИ ПРОИЗВОДСТВА ДРЕВЕСНЫХ ПЕЛЛЕТ И ЭКО МАЙНИНГА', 'preico' );?></p>

					<div class="clear-pads col-md-12 col-sm-12 col-xs-12 documents rounded">
						<?php if( get_field( 'documents' ) ){
							while(has_sub_field('documents')):?>
								<div class="col-lg-4 col-md-6 col-sm-6 ">
									<?php $file = get_sub_field('document_file'); $mime = wp_check_filetype( $file );?>
									<a target="_blank" href="<?php echo $file;?>">
									<div class="row document white-bg">
										<?php if( get_sub_field( 'document_file' )){ ?>

												<?php if( get_sub_field( 'document_icon' )){ ?>
													<img src="<?php echo get_sub_field('document_icon');?>">
												<?php } else { ?>
													<div class="flex">
														<img src="<?php bloginfo('template_directory');?>/resources/images/icons/address.png">
													</div>

												<?php } ?>

												<?php if( get_sub_field( 'document_title' )){ ?>
													<div class="description">
														<p class="download-title"><?php echo get_sub_field('document_title');?></p>
														<p class="download-this"><?php echo  __( 'Скачать', 'preico' );?>
															<?php if( isset( $mime[ 'ext' ] ) ){
																echo $mime[ 'ext' ];
															};?>

														</p>
													</div>

												<?php } else { ?>
													<p>|<?php __( 'Document', 'preico' ) ?> </p>
												<?php } ?>

										<?php } ?>
									</div>
						</a>
								</div>
							<?php endwhile;
						} ?>
					</div>


					<!--<div class="clear-pads col-md-12 col-sm-12 col-xs-12  documents">
						<?php if( get_field( 'external_links' ) ){
							while(has_sub_field('external_links')):?>
								<div class="col-md-12 yellow">
									<div class="row document">
										<?php if( get_sub_field( 'link_url' )){ ?>
											<a href="<?php echo get_sub_field('link_url');?>">
												<?php if( get_sub_field( 'link_name' )){ ?>
													<span><?php echo get_sub_field('link_name');?></span>
												<?php } else { ?>
													<span><?php echo get_sub_field('link_url');?></span>
												<?php } ?>
											</a>
										<?php } ?>
									</div>
								</div>
							<?php endwhile;
						} ?>
					</div>-->
					
				</div>
				
				
			</div>
		</div>
		
	</section>
	
	<!-- Section Promo -->
	<section class="section-promo">
		<div class="container">
			<div class="row">
				<div class="wrap-promo">
					<?php if (!is_user_logged_in()){ ?>
						<p><?php echo  __( 'Чтобы ознакомиться с полным перечнем документов необходима регистрация, для доступа в Личный Кабинет', 'preico' );?></p>
						<a href="<?php echo site_url(); ?><?php echo wpml_site_link(); ?>registration/" class="btn btn-yellow"><?php echo  __( 'РЕГИСТРАЦИЯ', 'preico' );?></a>
					<?php } else { ?>
						<p><?php echo  __( 'Чтобы ознакомиться с полным перечнем документов перейдите в Личный Кабинет', 'preico' );?></p>
						<a href="<?php echo site_url(); ?><?php echo wpml_site_link(); ?>private-office/" class="btn btn-yellow"><?php echo  __( 'Личный кабинет', 'preico' );?></a>
					<?php } ?>
				</div>
			</div>
		</div>
	</section><!-- /.section-promo -->	
	
	<section class="document-template">
	
		<div id="map"></div>
		
	</section>
	
	<!-- Section timer -->
	<section class="section-timer">
		<div class="container">
			<div class="row">
				<h3><?php echo  __( 'До конца PRE ICO', 'preico' );?></h3>
				
				<div class="counter-wrap">
					<div class="days dark-bg">
						<div class="counter-number">00</div>
						<div class="counter-text" style="display: block;"><?php echo  __( 'Days', 'preico' );?></div>
					</div>
					<div class="hours dark-bg">
						<div class="counter-number" style="display: block;">00</div>
						<div class="counter-text" style="display: block;"><?php echo  __( 'Hours', 'preico' );?></div>
					</div>
					<div class="minutes dark-bg">
						<div class="counter-number" style="display: block;">00</div>
						<div class="counter-text" style="display: block;"><?php echo  __( 'Minutes', 'preico' );?></div>
					</div>
					<div class="seconds dark-bg">
						<div class="counter-number" style="display: block; opacity: 0.15964;">00</div>
						<div class="counter-text" style="display: block;"><?php echo  __( 'Seconds', 'preico' );?></div>
					</div>
				</div>
			</div>
		</div>
	</section><!-- /.section-timer -->
	
	
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZEaLSoCJuIer6-0P38ypVtB3-zJX2SJE&libraries=places&language=en" async defer></script>
	
	
<?php get_footer('documents'); ?>

