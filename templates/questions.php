<?php
/**
 * Template Name: Questions
 */
get_header(); ?>

    <section  id="questions">

        <div class="container">

            <div class="question-wrap col-md-offset-2 col-md-8 col-sm-offset-1 col-sm-10 col-xs-offset-0 col-xs-12">
                <?php
                $args = array(
                    'post_type'      => 'questions',
                    'post_status'    => 'publish',
                    'orderby'        => 'post_date',
                    'order'          => 'DESC',
					'posts_per_page' => '-1'
                );
                $posts = new WP_Query( $args );

                if ( $posts->have_posts() ) : ?>

                <?php while ( $posts->have_posts() ) : $posts->the_post(); ?>

                    <a class="collapsed" data-toggle="collapse" href="#id<?php echo get_the_ID();?>" role="button" aria-expanded="false" aria-controls="id<?php echo get_the_ID();?>">
                        <div class="question-toggle">
                            <div class="col-md-12 col-sm-12 col-xs-12 question-title"> <?php the_title(); ?></div>

                            <div>
                                <img class="bullet" src="<?php bloginfo('template_directory');?>/resources/images/yellow-arrow.png">
                            </div>
                        </div>
                    </a>

                    <div class="collapse" id="id<?php echo get_the_ID();?>">
                        <div class="questions">
                            <?php the_content(); ?>
                        </div>
                    </div>

                <?php endwhile; ?>
                <?php wp_reset_query(); ?>
                <?php endif; ?>
            </div>
        </div>
    </section>


<?php get_footer('faq'); ?>