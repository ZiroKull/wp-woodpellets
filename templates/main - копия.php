<?php
/**
 * Template Name: Main
 */
get_header(); ?>

	<!-- Fixed navbar -->
	<nav id="header" class="navbar navbar-fixed-top">
		<div id="header-container" class="container navbar-container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a id="brand" class="navbar-brand" href="<?php echo site_url(); ?>">
					<img src="<?php bloginfo('template_directory');?>/img/logo.png" alt="">
				</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse pull-right-lg">
				<ul class="nav navbar-nav">
					<li class="active"><a href="<?php echo site_url(); ?>/calculator">Калькулятор</a></li>
					<li><a href="<?php echo site_url(); ?>/documents">Документация</a></li>
					<li><a href="#team">Команда</a></li>
					<li><a href="<?php echo site_url(); ?>/questions">Вопросы</a></li>
					<li><a href="#contact">Контакты</a></li>
					
					<?php if( is_user_logged_in() ){ ?>
						<li><a href="<?php echo site_url(); ?>/private-office/">Кабинет</a></li>
					<?php }else { ?>
						<li><a href="<?php echo site_url(); ?>/registration/">Регистрация</a></li>
						<li><a href="<?php echo site_url(); ?>/login/">Кабинет</a></li>
					<?php } ?>
					
					<!--
					<li>
						<div class="dropdown">
							<button class="dropdown-toggle" type="button" data-toggle="dropdown">
								<span class="locale">RU</span>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="">RU</a></li>
								<li><a href="">EN</a></li>
							</ul>
						</div>
					</li>-->
				</ul>
			</div><!-- /.nav-collapse -->
		</div><!-- /.container -->
	</nav><!-- /.navbar -->
	
	<!-- Section top -->
	<section id="wpclub" class="section-top">
		<div class="container">
			<div class="row">
				
				<div class="col-md-6 col-sm-6 col-xs-12 left-side">
					<div class="white-paper-wrap">
						<p class="yellow-text">PRE ICO</p>
						<a href="<?php echo site_url(); ?>/documentation/white-paper.pdf" class="white-paper" target="_blank">White Paper</a>
					</div>
					<h1>Проект Wood Pellets</h1>
					<p class="ico-desc">БЛОКЧЕИН - ОПЦИОН В ИНДУСТРИИ ПРОИЗВОДСТВА ДРЕВЕСНЫХ ПЕЛЛЕТ И ЭКОМАЙНИНГА</p>
					<div class="recycle">
						<img src="<?php bloginfo('template_directory');?>/img/vsobj.png">
						<span>100%</span>
					</div>
					
					<p class="ico-desc-bot">экологически чистое безотходное производство замкнутого цикла из возобновляемых источников энергии и применением новейших ИТ технологий</p>
					
					<div class="welcome">
						<a href="#section-progress" class="btn btn-yellow" >УЧАСТВОВАТЬ</a>
						<a href="<?php echo site_url(); ?>/calculator" class="btn btn-club margin-0" >КАЛЬКУЛЯТОР</a>
					</div>

				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-12 right-side">
					<img class="circle" src="<?php bloginfo('template_directory');?>/img/circle2.png">
					<div class="titles">
						<div class="top-first">
							<p class="first-title">Когенерационная станция<br>электрическая мощность 10 МВТ<br>тепловая мощность 27,7 МВТ</p>
						</div>
						<div class="top-second">
							<p class="second-title">Завод древесных пеллет<br>мощность 120 000 тонн/год</p>
							<p class="third-title">Дата - Центр для Mайнинга<br>10 МВТ 84375 TH/s</p>
						</div>
						<div class="bottom-first">
							<p class="fourth-title">Лесозавод<br>мощность 120 000 м3/год</p>
							<p class="fifth-title">Tепличный комплекс<br>1 млн саженцев ели/год</p>
						</div>
						<div class="bottom-second">
							<p class="sixth-title">Лесные ресурсы - площадь лесного фонда 71 967 га</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section><!-- /.section-top -->

	<!-- Section timer -->
	<section class="section-timer">
		<div class="container">
			<div class="row">
				<h3>До конца PRE ICO</h3>
				
				<div class="counter-wrap">
					<div class="days dark-bg">
						<div class="counter-number">60</div>
						<div class="counter-text" style="display: block;">Days</div>
					</div>
					<div class="hours dark-bg">
						<div class="counter-number" style="display: block;">4</div>
						<div class="counter-text" style="display: block;">Hours</div>
					</div>
					<div class="minutes dark-bg">
						<div class="counter-number" style="display: block;">30</div>
						<div class="counter-text" style="display: block;">Minutes</div>
					</div>
					<div class="seconds dark-bg">
						<div class="counter-number" style="display: block; opacity: 0.15964;">44</div>
						<div class="counter-text" style="display: block;">Seconds</div>
					</div>
				</div>
			</div>
		</div>
	</section><!-- /.section-timer -->
	
	<!-- Section Progress -->
	<section class="section-progress" id="section-progress">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12 left-prog">
					<div class="top-title">
						<p class="first">Soft Cap</p>
						<p class="second">250 000 EUR</p>
					</div>
					<div class="progress">
						<svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="373" height="373" xmlns="http://www.w3.org/2000/svg">
							<circle class="circle-chart__background" stroke="#46401c" stroke-width="1" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
							<circle class="circle-chart__circle" stroke="#ffd334" stroke-width="1" stroke-dasharray="0,100" stroke-linecap="" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
						</svg>
					</div>
					<div class="bottom-title">
						<p class="first">Hard Cap</p>
						<p class="second">1 000 000 EUR</p>
						<p class="curently-recived">Получено инвестиций</p>
						<p class="curently-recived-value">14150.13 EUR на 02.02.2018 08:20 MSK (UTC+3)</p>
					</div>
					
				</div>
			
				<div class="col-md-6 col-sm-6 col-xs-12 right-prog">
					<p>Чтобы стать членом клуба инвестора WPClub вы должны приобрести не менее 500 токенов. Какие ждут привилегии для участников клуба, вы можете ознакомиться на <a class="wallet-desc-link" href="<?php echo site_url(); ?>/club/">странице клуба</a></p>
					<label for="calc">Введите желаемую сумму токенов:</label>
					<form class="form-inline">
						<div class="form-group">
						  <input type="text"  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/^0+/, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" class="form-control" id="calc" value="100">
						</div>
						<button class="btn btn-success btn-green calculate">Расcчитать</button>
						<div class="form-group">
							<p>СКИДКА 75%</p>
						</div>
					</form>
					<p class="eth-price">Стоимость 1 ETH = <span class="eth_txt">769.93 EUR на 02.02.2018 08:20 MSK (UTC+3)</span></p>
					<p class="must_pay">Вы должны оплатить <span>0.00000000 ETH (0.00 EUR)</span></p>
					
					<p class="wallet">Номер кошелька</p>
					<button class="btn btn-success btn-green copy" data-clipboard-action="copy" data-clipboard-target="#wallet-value">Копировать</button>
					<button class="btn btn-success btn-green openqr" data-toggle="modal" data-target="#qrModal">Oткрыть QR код</button>
					<input id="wallet-value" class="wallet-value" value="01x000000000051456165165615561100">
					<a href="<?php echo site_url(); ?>/documentation/wallet.pdf" class="wallet-desc-link" target="_blank">Инструкция "Как создать эфир кошелек?"</a>
					<br>
					<a href="<?php echo site_url(); ?>/documentation/terms-and-conditions.pdf" class="wallet-desc-link" target="_blank">Условия и Положения!</a>
					
					<p class="wallet-desc-title">Для инвестирования, отправьте Эфир на официальный смарт контракт краудсейла.Нельзя переводить с обменников, только ETH кошельки. При транзакции, пожалуйста, ставте GAS LIMIT 200000.</p>
				
					<div class="divided-blocks">
						<div class="col-md-6 col-sm-12 col-xs-6 clear-pads border-right">
							<p class="font20">Soft Cap<br><span class="bold">0 - 250 000 EUR</span></p>
							<div class="col-md-12 clear-pads opts">
								<div class="black yellow-bg bold">СКИДКА 75%</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-6 no-pad-right">
							<p class="font20">Hard Cap<br><span class="bold">250 000 - 1 000 000 EUR</span></p>
							<div class="col-md-12 clear-pads opts">
								<div class="black yellow-bg bold">СКИДКА 50%</div>
							</div>
						</div>
					</div>
					<div class="token-wrap">
						<p class="token-title">Обратный выкуп по регламенту компании</p>
						<p class="token">1 Euro = <span>1 WPC</span></p>
					</div>
				</div>
				
				<div class="modal fade" id="qrModal" role="dialog">
					<div class="modal-dialog">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-body"><button type="button" class="close" data-dismiss="modal">&times;</button>
								<img src="<?php bloginfo('template_directory');?>/img/qr.jpg" width="100%">
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section><!-- /.section-progress -->
	
	<!-- Section Info 
	<section class="section-info">
		<div class="container">
			<div class="row">
				
			</div>
		</div>
	</section> /.section-info -->
	
	<!-- Section Problem -->
	<section class="section-problem padding-t-10">
		<div class="container">
			<div class="row">
				<h3 class="title">Всеобщая проблема</h3>
				
				<div class="col-md-6 text-block">
					Лес - это источник сырья для всех древесных изготовляющих или перерабатывающих предприятий . Множество лесозаготовительных и деревообрабатывающих предприятий после выполнения работ оставляют около 25 – 40 % отходного лесного и древесного материала, дальнейшая судьба которого просто уничтожается методом сжигания , поэтому так важна , переработка отходов древесины , её использование в производстве не только сохранит первичный древесный материал, но и уменьшит отходы древесины, что позволит значительно сократить вырубку лесов . Рассматривая проблемы лесопромышленного комплекса , специалисты постоянно заявляют о необходимости развития глубокой переработки древесного сырья.		
				</div>
				
				<div class="col-md-6 text-block">
					Несмотря на то, что Россия обладает четвертью мирового лесного фонда, на ее долю приходится лишь 3% мирового рынка продуктов глубокой переработки. Лишь 15% - 20% древесины в России подвергается глубокой переработке . Мы живем в мире где человечество равнодушно относится к нашей планете и экологии . Как объясняют исследователи , углекислый газ (CO2) в атмосфере Земли в 2016 году достиг максимальной концентрации . Его содержание увеличилось на 50% по сравнению со средним уровнем в течение последних 10 лет . Бизнес модель крипто майнинга в значительной степени зависит от источников энергии . Цена и доступность электроэнергии являются двумя важнейшими факторами для компаний , занимающихся майнингом .
				</div>
			</div>
		</div>
	</section>
	
	<section id="project">
		<div class="container">
			<h3 class="title"><?php echo  __( 'Инвестировать в реальное предприятие', 'preico' );?></h3>
			<div class="col-md-6 col-sm-12 col-xs-12">
				<!--<a href="" class="" data-toggle="modal" data-target="#videoModal" > -->

					<div class="col-md-12 clear-pads video-preview" style="background: url(<?php bloginfo('template_directory');?>/img/video.png)no-repeat center center;
						-webkit-background-size: cover;
						-moz-background-size: cover;
						-o-background-size: cover;
						background-size: cover;">

					</div>
				<!--</a> -->


				<div class="modal fade" id="videoModal" role="dialog">
					<div class="modal-dialog">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-body"><button type="button" class="close" data-dismiss="modal">&times;</button>
								<iframe width="560" height="315" src="https://www.youtube.com/embed/dE5j709dm_g" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
								
								<!--<iframe

									src="<?php echo get_field( 'video_url' );?>"
									frameborder="0" gesture="media"
									allowfullscreen></iframe>-->
							</div>
						</div>

					</div>
				</div>
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12 text-block no-margin-padding">
				<?php echo  __( 'В основе проекта WOOD PELLETS - внедрение модели интенсивного лесопользования с улучшением породного и качественного состава лесного фонда , с примененим экологически чистого , безотходного производства на основе глубокой переработки древесины и биотоплива (древесных пеллет) без дополнительных негативных воздействий на окружающую среду , с использованием возобновляемых источников энергии для совместной выроботки электричества , тепла и холода на собственные нужды предприятия и запуска экомайнинг фермы , что представляет собой автоматизированное интегрированное производство замкнутого цикла, использующее собственную ресурсно-сырьевую базу', 'preico' );?>
			</div>

			
		</div>

	</section>
	
	<section class="section-problem">

		<div class="container">
			<div class="row">
				
				<div class="col-md-12 col-sm-12 col-xs-12 clear-pads posts-wrap ">	
					<div class="col-md-3 col-sm-6 post-wrap">
						<div>
							<div class="description-post-img" style="background: url(<?php bloginfo('template_directory');?>/img/icons/boption.png)no-repeat center center;
									-webkit-background-size: contain;
									-moz-background-size: contain;
									-o-background-size: contain;
									background-size: contain;">
							</div>
						</div>
						<h4>Блокчейн — опцион</h4>
						<div class="posts-desc">
							<p>WPCoin это блокчейн — опцион для инвестирования в высокотехнологичный лесопромышленный &nbsp;комплекс.</p>
						</div>
					</div>
					
					<div class="col-md-3 col-sm-6 post-wrap">
						<div>
							<div class="description-post-img" style="background: url(<?php bloginfo('template_directory');?>/img/icons/pribil.png)no-repeat center center;
									-webkit-background-size: contain;
									-moz-background-size: contain;
									-o-background-size: contain;
									background-size: contain;">
							</div>
						</div>
						<h4>Гарантируемая прибыль</h4>
						<div class="posts-desc">
							<p>Инвестирование в WPCoin на ранних стадиях проекта позволит получить от 300 % прибыли.</p>
						</div>
					</div>
					
					<div class="col-md-3 col-sm-6 post-wrap">
						<div>
							<div class="description-post-img" style="background: url(<?php bloginfo('template_directory');?>/img/icons/product.png)no-repeat center center;
									-webkit-background-size: contain;
									-moz-background-size: contain;
									-o-background-size: contain;
									background-size: contain;">
							</div>
						</div>
						<h4>Готовая продукция</h4>
						<div class="posts-desc">
							<p>Каждый инвестор может получить готовую продукцию WPCoin. Обменяв WPC на соответствующие количество древесных гранул, исходя из расчета 1 тонна древесных пеллет на 100 WPCoin , на условиях FCA.</p>
						</div>
					</div>
					
					<div class="col-md-3 col-sm-6 post-wrap">
						<div>
							<div class="description-post-img" style="background: url(<?php bloginfo('template_directory');?>/img/icons/cache.png)no-repeat center center;
									-webkit-background-size: contain;
									-moz-background-size: contain;
									-o-background-size: contain;
									background-size: contain;">
							</div>
						</div>
						<h4>Выплаты</h4>
						<div class="posts-desc">
							<p>Выплаты будут производиться ежемесячно в криптовалюте Etherium (Eth), на кошелек с которого была произведена инвестиция, что даёт высокие гарантии получения вышей прибыли вовремя.</p>
						</div>
					</div>
					
					<div class="col-md-3 col-sm-6 post-wrap">
						<div>
							<div class="description-post-img" style="background: url(<?php bloginfo('template_directory');?>/img/icons/spros.png)no-repeat center center;
									-webkit-background-size: contain;
									-moz-background-size: contain;
									-o-background-size: contain;
									background-size: contain;">
							</div>
						</div>
						<h4>Спрос</h4>
						<div class="posts-desc">
							<p>Высокий спрос древесных пеллет обеспечен законодательством развитых стран в области экологии. Рост спроса обусловлен вступлением в силу Киотского протокола.</p>
						</div>
					</div>
					
					<div class="col-md-3 col-sm-6 post-wrap">
						<div>
							<div class="description-post-img" style="background: url(<?php bloginfo('template_directory');?>/img/icons/vector-smart-object-19@2x.png)no-repeat center center;
									-webkit-background-size: contain;
									-moz-background-size: contain;
									-o-background-size: contain;
									background-size: contain;">
							</div>
						</div>
						<h4>Конпенсация</h4>
						<div class="posts-desc">
							<p>Специальная программа Правительство Российской Федерации компенсирует до 80 % фактически понесённыех затрат при транспортировкие древесных пеллет на внешние  рынки до пункта назначения .</p>
						</div>
					</div>
					
					<div class="col-md-3 col-sm-6 post-wrap">
						<div>
							<div class="description-post-img" style="background: url(<?php bloginfo('template_directory');?>/img/icons/ecomaining.png)no-repeat center center;
									-webkit-background-size: contain;
									-moz-background-size: contain;
									-o-background-size: contain;
									background-size: contain;">
							</div>
						</div>
						<h4>Экомайнинг</h4>
						<div class="posts-desc">
							<p>Майнинг с использованием возобновляемых источников энергии на биомассе , где часть тепла отводится для создания холода (охлождения воды и кондиционирования ).</p>
						</div>
					</div>
					
					<div class="col-md-3 col-sm-6 post-wrap">
						<div>
							<div class="description-post-img" style="background: url(<?php bloginfo('template_directory');?>/img/icons/eth.png)no-repeat center center;
									-webkit-background-size: contain;
									-moz-background-size: contain;
									-o-background-size: contain;
									background-size: contain;">
							</div>
						</div>
						<h4>Etherium ERC20 </h4>
						<div class="posts-desc">
							<p>Наш токен разработан на платформе Etherium по стандартам ERC20 , что даёт нам возможность размещать  токен на популярных биржах.</p>
						</div>
					</div>
				</div>
		
			</div>
		</div>
		
		<div class="container-fluid margin-t-20">
			<div class="row">
				<div class="city-image col-md-12 col-sm-12 col-xs-12 clear-pads hidden-xs"></div>
			</div>
		</div>
		
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12 clear-pads">

					<div class="col-md-2 col-sm-4 col-xs-6 full-width-xs">
						<div class="dark-bg triangle-top" data-toggle="modal" data-target="#thirdModal">
							<h4 class="yellow">Лесные ресурсы</h4>
							<p class="white">площадь лесного фонда 71 967 га</p>
						</div>

						<div class="modal fade" id="thirdModal" role="dialog">
							<div class="modal-dialog">
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-body"><button type="button" class="close" data-dismiss="modal">×</button>
										<h4 class="yellow">Лесные ресурсы</h4>
										<p class="white">Компания является арендатором по 2 договорам аренды лесных участков в Псковской области, расположенных на землях лесного фонда,.. Срок аренды - 49 лет Ресурсная  база компании 8 000 000 кубометров древесины . Площадь   арендованного лесного фонда составляет 71 967 га, в том числе:  Опочецкое лесничество – 29 629 га.  Себежское лесничество – 42 338 га.. . Также имеет в  собственности производственную площадку с капитальными строениями общей площадью 40000 кв метров, с электрической мощностью около 1 МВт. Данная территория имеет хорошо развитую транспортную инфраструктуру. По ней проходят такие транспортные магистрали как: Москва-Балтия; Санкт-Петербург-Киев; Опочка-Полоцк, а также имеется железная дорога направлением Москва-Балтия, включающую в себя станцию «Себеж», на которой компания имеем железнодорожные пути с погрузочно-разгрузочными площадками. Сеть лесных дорог поддерживается в хорошем состоянии, позволяет вести заготовку леса круглогодично. Компания осуществляет активный экологический и противопожарный мониторинг, защиту лесов на предоставленных в аренду лесных участках и прилегающих территориях, тем самым обеспечивая защиту своего сырья. Компания ежегодно, по согласованию с Правительством Псковской области, высаживают несколько миллионов саженцев хвойных и лиственных пород деревьев и осуществляют авторский надзор за произведенными посадками.</p>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-2 col-sm-4 col-xs-6 full-width-xs">
						<div class="dark-bg triangle-top" data-toggle="modal" data-target="#firstModal">
							<h4 class="yellow">Лесозавод</h4>
							<p class="white">мощность 120 000 м3/год</p>
						</div>

						<div class="modal fade" id="firstModal" role="dialog">
							<div class="modal-dialog">
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-body"><button type="button" class="close" data-dismiss="modal">×</button>
										<h4 class="yellow">Лесозавод</h4>
										<p class="white">Лесопильный завод с производительностью пиления 100.000 – 120.000 м3 пиловочника в год в две смены в зависимости от карты распила и среднего диаметра бревна. Предлагаемая концепция на базе Фрезерно-ленточнопильной (Quadro) технологии предусматривает распиловку пиловочника по программно- устанавливаемым схемам раскроя на центральные и боковые доски (до макс. 8 боковых досок/брёвeн) в автоматическом режиме. Дополнительным преимуществом Линии является ее компактность при минимальной потребности в персонале (2-3 оператора в одну смену) на лесопильной линии и высокий коэффициент использования при низком удельном энергопотреблении на 1 м3 распиливаемого сырья.</p>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-2 col-sm-4 col-xs-6 full-width-xs">
						<div class="dark-bg triangle-top" data-toggle="modal" data-target="#fourthModal">
							<h4 class="yellow">Завод древесных пеллет</h4>
							<p class="white">мощность 120 000 тонн/год</p>
						</div>

						<div class="modal fade" id="fourthModal" role="dialog">
							<div class="modal-dialog">
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-body"><button type="button" class="close" data-dismiss="modal">×</button>
										<h4 class="yellow">Завод древесных пеллет</h4>
										<p class="white">Завод древесных пеллет мощностью 120 000 тонн в год  для производства древесно топливных гранул (пеллеты, ДТГ)Класса Enplus A1, Enplus A2.  – это небольшие цилиндрические прессованные древесные изделия диаметром 4-12 мм, длиной 20-50 мм, переработанные из высушенных остатков деревообрабатывающего и лесопильного производства: опилки, стружка, древесная мука, щепа, древесная пыль и т.д. Гранулы используются в котлах для получения тепловой и электрической энергии путем сжигания. Преимуществом использования древесных гранул перед другими видами топлива является:  снижение вредных выбросов СО2 в атмосферу.</p>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-2 col-sm-4 col-xs-6 full-width-xs">
						<div class="dark-bg triangle-top" data-toggle="modal" data-target="#secondModal">
							<h4 class="yellow">Когенерационная станция</h4>
							<p class="white">электрическая мощность 10 МВТ<br>тепловая мощность 27,7 МВТ</p>
						</div>

						<div class="modal fade" id="secondModal" role="dialog">
							<div class="modal-dialog">
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-body"><button type="button" class="close" data-dismiss="modal">×</button>
										<h4 class="yellow">Когенерация</h4>
										<p class="white">Когенерация это процесс совместной выработки электрической и тепловой энергии.  Отходы, образующиеся в процессе лесозаготовки и  производства, используются когенерационной станцией для выработки электро- и теплоэнергии на собственные нужды предприятия. таким образом комплекс заводов будет полностью независимым. Когенерационная установка является эффективной альтернативой тепловым сетям, благодаря гибкому изменению параметров теплоносителя в зависимости от требований потребителя в любое время года. Потребитель, имеющий в эксплуатации когенераторную электростанцию не подвержен зависимости от экономического состояния дел больших теплоэнергетических компаниях. Доход (или экономия) от реализации электричества и тепловой энергии, за короткое время, покрывают все расходы на когенераторную электростанцию. Окупаемость капитальных вложений в когенераторную установку происходит быстрее окупаемости средств, затраченных на подключение к электро и тепловым сетям, обеспечивая тем самым, устойчивый возврат инвестиций.</p>
									</div>
								</div>

							</div>
						</div>
					</div>

					<div class="col-md-2 col-sm-4 col-xs-6 full-width-xs">
						<div class="dark-bg triangle-top" data-toggle="modal" data-target="#fifthModal">
							<h4 class="yellow">Mайнинг фермa</h4>
							<p class="white">6750 Th/s</p>
						</div>

						<div class="modal fade" id="fifthModal" role="dialog">
							<div class="modal-dialog">
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-body"><button type="button" class="close" data-dismiss="modal">×</button>
										<h4 class="yellow">Mайнинг фермa</h4>
										<p class="white">При всей прибыльности майнинга, самым большим источником затрат для процесса майнинга остаются расходы на электроэнергию.
	В будущем майнинг прекратит свое существование в регионах с высокими ценами на энергоносители . В большинстве стран Европы производство возобновляемых источниках энергии в значительной степени субсидируется, в результате чего цена такой электроэнергии составляет от 6 до 8 центов за кВт. Поэтому Евро Союз станет первым, где майнинг перестанет приносить прибыль. Таким образом, лучшим способом сохранения конкурентоспособности является применение недорогих источников электроэнергии – экологически чистых и не субсидируемых государством.
	Когенерация в производственном комплексе  считается одним из самых эффективных и экономичных источников возобновляемой энергии. Данный вид электроэнергии является экологически чистым, с точки зрения выбросов углекислого газа. Использование электроэнергии, которая производится, позволяет сделать цену одного киловатта одной их самых низких в России. 
	Кроме того,  также можно  использовать генерацию , где часть тепла со средней температурой (100-200 градусов ) отводится в Абсорбционную холодильную машину для создания холода .Холод используется для охлаждения воды или в системах кондиционирования, что позволит увеличить мощность  , по сравнению с воздушным охлаждением .
	 Таким образом, майнинг с использованием возобновляемых источниках энергии на биомассе ,   является не только экологически чистым, но и прибыльным.</p>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-2 col-sm-4 col-xs-6 full-width-xs">
						<div class="dark-bg triangle-top" data-toggle="modal" data-target="#sixthModal">
							<h4 class="yellow">Тепличный комплекс</h4>
							<p class="white">2 млн саженцев ели /год</p>
						</div>

						<div class="modal fade" id="sixthModal" role="dialog">
							<div class="modal-dialog">
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-body"><button type="button" class="close" data-dismiss="modal">×</button>
										<h4 class="yellow">Тепличный комплекс</h4>
										<p class="white">Чтобы обеспечить качественным сырьем проводимые лесовосстановительные работы , будет возведения современного  тепличного  леса комплекса, где  произведенная когенерационной станцией, тепловая энергия будет также подается в питомник  , где будут выращивать саженцы деревьев хвойных пород . Построенный питомник предназначен для выращивания сеянцев с закрытой корневой системой. 
	Такой способ выращивания посадочного материала позволяет получать стопроцентную приживаемость, ускоренный рост в первые годы, возможность существенно продлить периоды посадки и сменить сроки посадки на начало лета — осень для снижения напряжённости посадочных работ весной.	Планируемый выход посадочного материала составляет 1 млн штук сеянцев ели. Что позволит восстанавливать 1000 га утраченного лесного фонда по хвойному хозяйству Псковской области ежегодно.</p>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		
	</section><!-- /.section-problem -->
	
	<!-- Section Club -->
	<section class="section-club">
		<div class="container">
			<div class="row">
				<h3 class="title white">Программа участия в клубе  WP</h3>
				
				<div class="club-wrap">
					<div class="col-md-3 col-sm-6 col-xs-12 center border-club">
						<img src="<?php bloginfo('template_directory');?>/img/wp-bronze.png">
						
						<p class="club-wp-desc">> 500 WPC</p>
					</div>
					
					<div class="col-md-3 col-sm-6 col-xs-12 center border-club">
						<img src="<?php bloginfo('template_directory');?>/img/wp-silver.png">
						
						<p class="club-wp-desc">> 5 000 WPC</p>
					</div>
					
					<div class="col-md-3 col-sm-6 col-xs-12 center border-club">
						<img src="<?php bloginfo('template_directory');?>/img/wp-gold.png">
						
						<p class="club-wp-desc">> 50 000 WPC</p>
					</div>
					
					<div class="col-md-3 col-sm-6 col-xs-12 center">
						<img src="<?php bloginfo('template_directory');?>/img/wp-premium.png">
						
						<p class="club-wp-desc">> 500 000 WPC</p>
					</div>
				</div>
				
				<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 club-text">
					<p>Инвестируй в реальный бизнес и получай процент от дохода предприятий, ознакомиться с условиями клубной программы можно на странице WPC клуб.</p>
				</div>
				
				<div class="center club-buttons">
					<a href="<?php echo site_url(); ?>/club" class="btn btn-club">ОЗНАКОМИТЬСЯ</a>
					<a href="" class="btn btn-yellow" data-toggle="modal" data-target="#registrationModal">РЕГИСТРАЦИЯ</a>
				</div>	
			</div>
		</div>
	</section><!-- /.section-club -->
	
	<section id="news">
		<div class="container border-bottom">

			<h3 class="title"><?php echo __( 'Новости', 'preico' ) ?></h3>
			
			<?php
				$args = array(
					'post_type'      => 'post',
					'post_status'    => 'publish',
					'orderby'        => 'post_date',
					'order'          => 'DESC',
					'posts_per_page' => '4'
				);
				$posts = new WP_Query( $args );

				if ( $posts->have_posts() ) : ?>

					<?php $num = 0; ?>
						<?php while ( $posts->have_posts() ) : $posts->the_post(); ?>

							<div class="col-md-3 col-sm-6 col-xs-12 team-member" style="display: block;">
							<?php $image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() )); ?>
								<?php if($image) { ?>
									<img src="<?php echo $image; ?>" class="img-thumbnail" width="250" height="150">
								<?php } else { ?>
									<img src="<?php bloginfo('template_directory');?>/img/default.png" class="img-thumbnail" width="250" height="150">
								<?php } ?>
								<a href="<?php the_permalink(); ?>" class=""><?php the_title(); ?></a>
								<div class=""><?php the_content(); ?></div>
							</div>

						<?php $num++;?>
					<?php endwhile; ?>
				<?php wp_reset_query(); ?>
			<?php endif; ?>
			
			<div class="col-md-12 col-sm-12 col-xs-12 clear-pads text-center collapse-btn-container margin-t-20 margin-b-40">
				<a href="<?php echo site_url(); ?>/post-news" class="btn more-btn btn-club" role="button">
					<span class="more">Новости</span>
				</a>
			</div>
			
		</div>
	</section>
	
	<!-- Section Subscribe -->
	<section class="section-subscribe">
		<div class="container">
			<div class="row">
				<div class="subscription-container">

					<div class="col-md-6 col-sm-6 subscribe-description ">
						<div class="col-md-3 col-sm-12 clear-pads">
							<img src="<?php bloginfo('template_directory');?>/resources/images/mail.png">
						</div>
						<div class="col-md-9 col-sm-12">
							<h1><?php echo __( 'Подпишитесь', 'preico' ) ?></h1>
							<p><?php echo __( 'еженедельную подпискa новостей.
								Информация, которая будет отсилаться
								при подписке.', 'preico' ) ?>
							</p>
						</div>

					</div>
					<div class="col-md-6 col-sm-6">
						<?php es_subbox( $namefield = "YES", $desc = "", $group = "" ); ?>

					</div>
				</div>
			</div>
		</div>
	</section><!-- /.section-subscribe -->
	
	
	<section id="roadmap">
		<h1 class="title white"><?php echo __( 'Дорожная карта', 'preico' ) ?></h1>

		<div class="container">
			<div class="col-md-6 col-sm-6 left-block">
				<div class="roadmap-block first">
					<h4 class="yellow"><?php echo __( '2017 Q2-Q4 - Разработка проектной базы', 'preico' ) ?></h4>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/peregovor.png">

						</div>
						<p class="title-text"><?php echo __( 'Проведение переговоров с поставщиками', 'preico' ) ?></p>
					</div>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/zapusk_sajta.png">

						</div>
						<p class="title-text"><?php echo __( 'Запуск сайта', 'preico' ) ?></p>
					</div>
				</div>

				<div class="roadmap-block second hidden-lg hidden-md hidden-sm">
					<h4 class="yellow"><?php echo __( '2018 Q1  - PRE SALE', 'preico' ) ?></h4>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/marketing.png">

						</div>
						<p class="title-text"><?php echo __( 'Маркетинговая составляющая', 'preico' ) ?></p>
					</div>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/registracija.png">

						</div>
						<p class="title-text"><?php echo __( 'Пегистрационные процессы', 'preico' ) ?></p>
					</div>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/video.png">

						</div> <p class="title-text"><?php echo __( 'Установка видеонаблюдения', 'preico' ) ?></p>
					</div>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/rabota_s_investorami.png">

						</div><p class="title-text"><?php echo __( 'Работа с инвесторами', 'preico' ) ?></p>
					</div>
				</div>

				<div class="roadmap-block third">
					<h4 class="yellow"><?php echo __( '2018 Q2 - ICO', 'preico' ) ?></h4>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/podgotovitelnie_raboti.png">

						</div>
						<p class="title-text"><?php echo __( 'Подготовительные работы', 'preico' ) ?></p>
					</div>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/pokupka.png">

						</div>
						<p class="title-text"><?php echo __( 'Покупка техники', 'preico' ) ?></p>
					</div>
					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/stroitelstvo.png">

						</div><p class="title-text"><?php echo __( 'Начало строительных работ', 'preico' ) ?></p>

					</div>
					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/pokupka.png">

						</div>
						<p class="title-text"><?php echo __( 'Покупка производственного оборудования', 'preico' ) ?></p>
					</div>
				</div>

			</div>

			<div class="col-md-6 col-sm-6 right-block hidden-xs">
				<div class="roadmap-block second">
					<h4 class="yellow"><?php echo __( '2018 Q1  - PRE SALE', 'preico' ) ?> </h4>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/marketing.png">

						</div>
						<p class="title-text"><?php echo __( 'Маркетинговая составляющая', 'preico' ) ?></p>
					</div>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/registracija.png">

						</div>
						<p class="title-text"><?php echo __( 'Пегистрационные процессы', 'preico' ) ?></p>
					</div>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/video.png">

						</div> <p class="title-text"><?php echo __( 'Установка видеонаблюдения', 'preico' ) ?></p>
					</div>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/rabota_s_investorami.png">

						</div><p class="title-text"><?php echo __( 'Работа с инвесторами', 'preico' ) ?></p>
					</div>
				</div>
			</div>

			<div class="col-md-12 col-sm-12 bottom-block">
				<div class="roadmap-block fourth">
					<h4 class="yellow"><?php echo __( '2019 Q2-Q4 - Запуск комплекса', 'preico' ) ?></h4>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/nachalo_prodaz.png">

						</div><p class="title-text"><?php echo __( 'Начало продаж продукции компании', 'preico' ) ?></p>

					</div>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/token.png">

						</div><p class="title-text"><?php echo __( 'Начало выкупа токенов', 'preico' ) ?></p>

					</div>
				</div>
			</div>

		</div>

	</section>
	
	<section id="contact">
		<div class="container border-bottom">

			<h3 class="title"><?php echo __( 'Свяжитесь с нами', 'preico' ) ?></h3>
			
			<div class="col-md-6 col-sm-12 col-xs-12 form">
				<?php echo do_shortcode( '[contact-form-7 id="64" title="Связь с нами"]' );?>
			</div> 
			
			<div class="col-md-6 col-sm-12 col-xs-12 contactinfo">
				<div class="col-md-12 col-sm-6 clear-pads">
                    <div class="address">

                        <img class="address-icon" src="<?php bloginfo('template_directory');?>/resources/images/icons/address.png">
                        <p>182250, Псковцкая обл., Себежцкий п н г Себеж, ул. В И Марго д 32</p>
                    </div>
                    <div class="phone">

                        <img class="phone-icon" src="<?php bloginfo('template_directory');?>/resources/images/icons/phone.png">
                        <p>+ 371 29120386</p>
                    </div>
                </div>

                <div class="col-md-12 col-sm-6 clear-pads soc-icons">
					<a target="_blank" href="" class="soc vk soc2">
						<i class="fa fa-vk" aria-hidden="true"></i>
					</a>
					<a target="_blank" href="" class="soc fb soc2">
						<i class="fa fa-facebook" aria-hidden="true"></i>
					</a>
					<a target="_blank" href="" class="soc tl soc2">
						<i class="fa fa-paper-plane" aria-hidden="true"></i>
					</a>

                    <p class="margin-t-20"> <?php echo  __( 'Наши страницы в социальных сетях', 'preico' );?> </p>
                </div>
			</div> 
		</div>
	</section>
	
	<section id="team">
		<div class="container border-bottom">

			<h3 class="title"><?php echo __( 'Наша команда', 'preico' ) ?></h3>
			
			<?php
				$args = array(
					'post_type'      => 'team',
					'post_status'    => 'publish',
					'orderby'        => 'post_date',
					'order'          => 'DESC',
					'posts_per_page' => '4'
				);
				$posts = new WP_Query( $args );

				if ( $posts->have_posts() ) : ?>

						<?php while ( $posts->have_posts() ) : $posts->the_post(); ?>

							<div class="col-md-3 col-sm-6 col-xs-12 team-member team-member-block" style="display: block;">
							<?php $image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() )); ?>
								<?php if($image) { ?>
									<img src="<?php echo $image; ?>" class="img-circle" width="180" height="180">
								<?php } else { ?>
									<img src="<?php bloginfo('template_directory');?>/img/people.png" class="img-circle" width="180" height="180">
								<?php } ?>
								<h4 class=""><?php the_title(); ?></h4>
								<div class=""><?php the_content(); ?></div>
							</div>

					<?php endwhile; ?>
				<?php wp_reset_query(); ?>
			<?php endif; ?>
			
			<?php
				$args = array(
					'post_type'      => 'team',
					'post_status'    => 'publish',
					'orderby'        => 'post_date',
					'order'          => 'DESC',
					'offset'		 => '4',
				);
				$posts = new WP_Query( $args );

				if ( $posts->have_posts() ) : ?>

			<?php while ( $posts->have_posts() ) : $posts->the_post(); ?>

					<div class="col-md-3 col-sm-6 col-xs-12 team-member team-member-block" style="display: none;">
						<?php $image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() )); ?>
						<?php if($image) { ?>
							<img src="<?php echo $image; ?>" class="img-circle" width="180" height="180">
						<?php } else { ?>
							<img src="<?php bloginfo('template_directory');?>/img/people.png" class="img-circle" width="180" height="180">
						<?php } ?>
						<h4 class=""><?php the_title(); ?></h4>
						<div class=""><?php the_content(); ?></div>
					</div>

				<?php endwhile; ?>
				<?php wp_reset_query(); ?>
			<?php endif; ?>
				
				
			<div class="col-md-12 col-sm-12 col-xs-12 clear-pads text-center collapse-btn-container margin-b-40">
				<a class="btn more-btn btn-club" role="button">
					<span class="more"><?php echo __( 'Показать', 'preico' ) ?></span>
					<span class="less"><?php echo __( 'Скрыть', 'preico' ) ?></span>
				</a>
			</div>
		</div>
		
		
		
	</section>
	
	<section id="partners">
		<div class="container">
			<h3 class="title"><?php echo __( 'Партнеры', 'preico' ) ?></h3>

			<?php
			$args = array(
				'post_type'      => 'partner',
				'post_status'    => 'publish',
				'orderby'        => 'post_date',
				'order'          => 'DESC',
				'posts_per_page' => '-1'
			);
			$posts = new WP_Query( $args );

			if ( $posts->have_posts() ) : ?>
				<div class="partners-wrap col-md-12 clear-pads">
					<?php while ( $posts->have_posts() ) : $posts->the_post(); ?>

						<?php $image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() )); ?>
						<?php if($image) { ?>
							<div class="partner col-md-3 clear-pads" style="background: url(<?php echo $image; ?>)no-repeat center center;
								-webkit-background-size: contain;
								-moz-background-size: contain;
								-o-background-size: contain;
								background-size: contain;">
						<?php } ?>

						</div>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>

		</div>
	</section>
	
	<div id="scrollup" class="">
		<a href="#wpclub"></a>
	</div>
	
<?php get_footer(); ?>


