<?php
/**
 * Template Name: Club
 */
get_header(); ?>
	
	<!-- Section Club Top -->
	<section class="section-club-top">
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-sm-6 ">
					<h1><?php echo  __( 'Клуб WPC', 'preico' );?></h1>
					<p><?php echo  __( 'Участниками клуба становятся покупатели и держатели токенов WPC', 'preico' );?></p>
				</div>
			</div>
		</div>
	</section><!-- /.section-club-top -->
	
	<!-- Section Content -->
	<section class="section-content">
		<div class="container">
			<div class="row bottom-b-silver club-row-top">
			
				<?php
					$args = array(
						'post_type'      => 'club',
						'post_status'    => 'publish',
						'orderby'        => 'post_date',
						'order'          => 'ASC',
						'posts_per_page' => '2'
					);
					$posts = new WP_Query( $args );

					if ( $posts->have_posts() ) : ?>
			
						<?php while ( $posts->have_posts() ) : $posts->the_post(); ?>
						
							<?php $image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() )); ?>
							
							<div class="col-md-6 col-sm-6 club-item">
								<div class="club-left">
									<img src="<?php echo $image; ?>">
									
									<p class="bronze"><?php the_field('token-value'); ?></p>						
								</div>
								
								<div class="club-right">
									<h3><?php the_title(); ?></h3> 
									
									<p><?php the_content(); ?></p>
									<p class="yellow"><?php the_field('bonus'); ?></p>
								</div>
							</div>

						<?php 
							endwhile; 
						?>
					<?php wp_reset_query(); ?>
				<?php endif; ?>

			</div>
			
			<div class="row club-row-bottom">
			
				<?php
					$args = array(
						'post_type'      => 'club',
						'post_status'    => 'publish',
						'orderby'        => 'post_date',
						'order'          => 'ASC',
						'posts_per_page' => '2',
						'offset'         => '2'
					);
					$posts = new WP_Query( $args );

					if ( $posts->have_posts() ) : ?>
			
						<?php while ( $posts->have_posts() ) : $posts->the_post(); ?>
						
							<?php $image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() )); ?>
							
							<div class="col-md-6 col-sm-6 club-item">
								<div class="club-left">
									<img src="<?php echo $image; ?>">
									
									<p class="bronze"><?php the_field('token-value'); ?></p>						
								</div>
								
								<div class="club-right">
									<h3><?php the_title(); ?></h3> 
									
									<p><?php the_content(); ?></p>
									<p class="yellow"><?php the_field('bonus'); ?></p>
								</div>
							</div>

						<?php 
							endwhile; 
						?>
					<?php wp_reset_query(); ?>
				<?php endif; ?>
				
			</div>
			
			<div class="row bg-blue-gray">
				<div class="col-md-12">
					<p><?php echo  __( 'Начисление прибыли полностью автоматизированы и гарантируются самим смарт контрактом токена wpc.Выплаты будут производиться в криптовалюте Etherium (ETH) на кошелек держателя, в соответствии его клубного статуса по текущему курсу eth к евро за вычетом оплаты транзакции из кабинета инвестора.', 'preico' );?></p>
				</div>
			</div>
		</div>
	</section><!-- /.section-content -->
	
	<!-- Section Promo -->
	<section class="section-promo">
		<div class="container">
			<div class="row">
				<div class="wrap-promo">
					<p><?php echo  __( 'Инвестируй уже сейчас и стань членом клуба', 'preico' );?></p>
					<?php if (!is_user_logged_in()){ ?>
						<a href="<?php echo site_url(); ?><?php echo wpml_site_link(); ?>registration/" class="btn btn-yellow"><?php echo  __( 'РЕГИСТРАЦИЯ', 'preico' );?></a>
					<?php } ?>
				</div>
			</div>
		</div>
	</section><!-- /.section-promo -->	
	
<?php get_footer('club'); ?>


