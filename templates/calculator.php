<?php
/**
 * Template Name: Calculator
 */
get_header(); ?>
	
	<!-- Section Club Top -->
	<section class="section-club-top">
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-sm-6 ">
					<h1><?php echo  __( 'Калькулятор WPC', 'preico' );?></h1>
					<p><?php echo  __( 'Участниками клуба становятся покупатели и держатели токенов WPC', 'preico' );?></p>
				</div>
			</div>
		</div>
	</section><!-- /.section-club-top -->
	
	<!-- Section Content -->
	<section class="section-content calc-wrap">
		<div class="container">
			<div class="row bottom-b-silver club-row-top">
				<h3 class="title white"><?php echo  __( 'Калькулятор', 'preico' );?></h3>
				<p class="eth-price eth-calc-price"><?php echo  __( 'Стоимость', 'preico' );?> 1 ETH = <span class="eth_txt"><span class="value">0 EUR</span> <?php echo  __( 'на', 'preico' );?> <span class="time">02.04.2018 00:00 MSK (GMT+3)</span></span></p>
				<div class="calc__params">
					<div class="calc-params-invest">
						<p class="calc-params-invest__title"><?php echo  __( 'Ваши инвестиции', 'preico' );?></p>
						<div class="calc-params-invest__inp"><span class="calc-params-invest__val">€</span>
							<input class="calc-params-invest__price" id="investInput" type="text" value="100" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/^0+/, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
						</div>
						<input id="invests" type="range" min="0" max="250000" step="50" value="100">
					</div>
					<div class="calc-params-bonus">
						<p class="calc-params-bonus__title"><?php echo  __( 'Вы получите', 'preico' );?></p>
						<div class="calc-params-bonus__input">
							<p class="calc-params-bonus__invest" id="totalInvest">400</p><span>WPC</span>
						</div>
						<input id="sale" type="range" min="50" max="75" step="25" value="75" disabled>
						<p class="calc-params-bonus__bonus" id="saleValue">75%</p>
						<p class="calc-params-bonus__date" id="saleText">Soft Cap 0 - 250 000 €</p>
					</div> 
				</div>
				
				<div class="calc__result"> 
					<div class="calc-result-profit">
						<p class="calc-result-profit__title"><?php echo  __( 'Членство', 'preico' );?></p>
						<div class="col-md-12 col-sm-12 club-item club-item-begin" style="display: block;">
							<div class="club-left">
								<img src="<?php bloginfo('template_directory');?>/img/wp-begin.png">
								
								<p class="bronze">< 500 WPC </p>						
							</div>
							
							<div class="club-right">
								<h3><?php echo  __( 'Начальный', 'preico' );?></h3> 
								
								<p><?php echo  __( 'Держателям от 1 до 500 WPC.', 'preico' );?></p>
							</div>
						</div>
						
						<?php
							$args = array(
								'post_type'      => 'club',
								'post_status'    => 'publish',
								'orderby'        => 'post_date',
								'order'          => 'ASC',
								'posts_per_page' => '4'
							);
							$posts = new WP_Query( $args );
							
							$memb_type = array('bronze', 'silver', 'gold', 'premium');
							$num = 0;

							if ( $posts->have_posts() ) : ?>
					
								<?php while ( $posts->have_posts() ) : $posts->the_post(); ?>
								
									<?php $image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() )); ?>
									
									<div class="col-md-12 col-sm-12 club-item club-item-<?php echo $memb_type[$num]; ?>" style="display: none;">
										<div class="club-left">
											<img src="<?php echo $image; ?>">
											
											<p class="<?php echo $memb_type[$num]; ?>"><?php the_field('token-value'); ?></p>						
										</div>
										
										<div class="club-right">
											<h3><?php the_title(); ?></h3> 
											
											<p><?php the_content(); ?></p>
											<p class="yellow"><?php the_field('bonus'); ?></p>
										</div>
									</div>

								<?php 
									$num++;
									endwhile; 
								?>
							<?php wp_reset_query(); ?>
						<?php endif; ?>

					</div>
					
					<div class="calc-result-info">
						<div class="calc-result-info__item option"> 
							<div class="calc-result-info__item-pos"><?php echo  __( 'Опцион', 'preico' );?>:</div>
							<p class="calc-result-info__item-value" id="optionHtml"></p>
						</div>
						<div class="calc-result-info__item cacheback" style="display: none;">
							<div class="calc-result-info__item-pos"><?php echo  __( 'Кэшбэк', 'preico' );?>:</div>
							<p class="calc-result-info__item-value" id="cachebackHtml"></p>
						</div>
						<div class="calc-result-info__item dividends" style="display: none;">
							<div class="calc-result-info__item-pos"><?php echo  __( 'Дивиденды', 'preico' );?>:</div>
							<p class="calc-result-info__item-value" id="dividendsHtml"></p>
						</div>
						<div class="calc-result-info__item maining" style="display: none;">
							<div class="calc-result-info__item-pos"><?php echo  __( 'Майнинг', 'preico' );?>:</div>
							<p class="calc-result-info__item-value" id="mainingHtml"></p>
						</div>
						
						<div class="calc-result-info__item total calc-result-info__item--accent">
						  <div class="calc-result-info__item-pos"><?php echo  __( 'Итого в год', 'preico' );?>:</div>
						  <div class="calc-result-info__item-total"><span>€</span>
							<p class="calc-result-info__item-value" id="total"></p>
						  </div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row bg-blue-gray">
				<div class="col-md-12">
					<p><?php echo  __( 'Начисление прибыли полностью автоматизированы и гарантируются самим смарт контрактом токена wpc.Выплаты будут производиться в криптовалюте Etherium (ETH) на кошелек держателя, в соответствии его клубного статуса по текущему курсу eth к евро за вычетом оплаты транзакции из кабинета инвестора.', 'preico' );?></p>
				</div>
			</div>
		</div>
	</section><!-- /.section-content -->
	
	<!-- Section Promo -->
	<section class="section-promo">
		<div class="container">
			<div class="row">
				<div class="wrap-promo">
					<p><?php echo  __( 'Инвестируй уже сейчас и стань членом клуба', 'preico' );?></p>
					<?php if (!is_user_logged_in()){ ?>
						<a href="<?php echo site_url(); ?><?php echo wpml_site_link(); ?>registration/" class="btn btn-yellow"><?php echo  __( 'РЕГИСТРАЦИЯ', 'preico' );?></a>
					<?php } ?>
				</div>
			</div>
		</div>
	</section><!-- /.section-promo -->	
	
<?php get_footer('calculator'); ?>


