<section id="wpclub" class="section-top">
		<div class="container">
			<div class="row">
				
				<div class="col-md-6 col-sm-6 col-xs-12 left-side">
					<div class="white-paper-wrap">
						<p class="yellow-text"><?php echo  __( 'PRE ICO', 'preico' );?></p>
						<a href="<?php echo site_url(); ?>/documentation/white-paper.pdf" class="white-paper" target="_blank"><?php echo  __( 'White Paper', 'preico' );?></a>
					</div>
					<h1><?php echo  __( 'Проект Wood Pellets', 'preico' );?></h1>
					<p class="ico-desc"><?php echo  __( 'БЛОКЧЕЙН - ОПЦИОН В ИНДУСТРИИ ПРОИЗВОДСТВА ДРЕВЕСНЫХ ПЕЛЛЕТ И ЭКО МАЙНИНГА', 'preico' );?></p>
					<div class="recycle">
						<img class="hidden-xs" src="<?php bloginfo('template_directory');?>/img/vsobj.png">
						<img class="visible-xs" src="<?php bloginfo('template_directory');?>/img/vsobj-mb.png">
						<span>100%</span>
					</div>
					
					<p class="ico-desc-bot"><?php echo  __( 'экологически чистое безотходное производство замкнутого цикла из возобновляемых источников энергии и применением новейших ИТ технологий', 'preico' );?></p>
					
					<div class="welcome">
						<a href="#section-progress" class="btn btn-yellow" ><?php echo  __( 'УЧАСТВОВАТЬ', 'preico' );?></a>
						<a href="<?php echo site_url(); ?>/calculator" class="btn btn-club margin-0" ><?php echo  __( 'КАЛЬКУЛЯТОР', 'preico' );?></a>
					</div>

				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-12 right-side">
					<div class="hidden-xs">
						<img class="circle" src="<?php bloginfo('template_directory');?>/img/circle2.png">
						<div class="titles">
							<div class="top-first">
								<p class="first-title"><?php echo  __( 'Когенерационная станция<br>электрическая мощность 10 МВТ<br>тепловая мощность 27,7 МВТ', 'preico' );?></p>
							</div>
							<div class="top-second">
								<p class="second-title"><?php echo  __( 'Завод древесных пеллет<br>мощность 120 000 тонн/год', 'preico' );?></p>
								<p class="third-title"><?php echo  __( 'Дата - Центр для Mайнинга<br>10 МВТ 84375 TH/s', 'preico' );?></p>
							</div>
							<div class="bottom-first">
								<p class="fourth-title"><?php echo  __( 'Лесозавод<br>мощность 120 000 м3/год', 'preico' );?></p>
								<p class="fifth-title"><?php echo  __( 'Tепличный комплекс<br>1 млн саженцев ели/год', 'preico' );?></p>
							</div>
							<div class="bottom-second">
								<p class="sixth-title"><?php echo  __( 'Лесные ресурсы - площадь лесного фонда 71 967 га', 'preico' );?></p>
							</div>
						</div>
					</div>
					
					<div class="details-sm visible-xs">
						<div>
							<img src="<?php bloginfo('template_directory');?>/resources/images/wood.png">
							<p><?php echo  __( 'Лесные ресурсы - площадь лесного фонда 71 967 га', 'preico' );?></p>
							<img class="bullet" src="<?php bloginfo('template_directory');?>/resources/images/yellow-arrow-down.png">
						</div>

						<div>
							<img src="<?php bloginfo('template_directory');?>/resources/images/factory-2.png">
							<p><?php echo  __( 'Лесозавод<br>мощность 120 000 м3/год', 'preico' );?></p>
							<img class="bullet" src="<?php bloginfo('template_directory');?>/resources/images/yellow-arrow-down.png">
						</div>

						<div>
							<img src="<?php bloginfo('template_directory');?>/resources/images/factory.png">
							<p><?php echo  __( 'Завод древесных пеллет<br>мощность 120 000 тонн/год', 'preico' );?></p>
							<img class="bullet" src="<?php bloginfo('template_directory');?>/resources/images/yellow-arrow-down.png">
						</div>

						<div>
							<img src="<?php bloginfo('template_directory');?>/resources/images/electr-factory.png">
							<p><?php echo  __( 'Когенерационная станция<br>электрическая мощность 10 МВТ<br>тепловая мощность 27,7 МВТ', 'preico' );?></p>
							<img class="bullet" src="<?php bloginfo('template_directory');?>/resources/images/yellow-arrow-down.png">
						</div>

						<div>
							<img src="<?php bloginfo('template_directory');?>/resources/images/farm.png">
							<p><?php echo  __( 'Дата - Центр для Mайнинга<br>10 МВТ 84375 TH/s', 'preico' );?></p>
							<img class="bullet" src="<?php bloginfo('template_directory');?>/resources/images/yellow-arrow-down.png">
						</div>

						<div>
							<img src="<?php bloginfo('template_directory');?>/resources/images/warmhouse.png">
							<p><?php echo  __( 'Tепличный комплекс<br>1 млн саженцев ели/год', 'preico' );?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section><!-- /.section-top -->