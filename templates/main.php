<?php
/**
 * Template Name: Main
 */
get_header(); ?>
	
	<!-- Section top -->
	<section id="wpclub" class="section-top">
		<div class="container">
			<div class="row">
				
				<div class="col-md-6 col-sm-6 col-xs-12 left-side">
					<div class="white-paper-wrap">
						<p class="yellow-text"><?php echo  __( 'PRE ICO', 'preico' );?></p>
						<a href="<?php the_field('main_white_paper'); ?>" class="white-paper" target="_blank"><?php echo  __( 'White Paper', 'preico' );?></a>
					</div>
					<h1><?php echo  __( 'Проект Wood Pellets', 'preico' );?></h1>
					<p class="ico-desc"><?php echo  __( 'БЛОКЧЕЙН - ОПЦИОН В ИНДУСТРИИ ПРОИЗВОДСТВА ДРЕВЕСНЫХ ПЕЛЛЕТ И ЭКО МАЙНИНГА', 'preico' );?></p>
					<div class="recycle">
						<img class="hidden-xs" src="<?php bloginfo('template_directory');?>/img/vsobj.png">
						<img class="visible-xs" src="<?php bloginfo('template_directory');?>/img/vsobj-mb.png">
						<span>100%</span>
					</div>
					
					<p class="ico-desc-bot"><?php echo  __( 'экологически чистое безотходное производство замкнутого цикла из возобновляемых источников энергии и применением новейших ИТ технологий', 'preico' );?></p>
					
					<div class="welcome">
						<a href="<?php echo site_url(); ?><?php echo wpml_site_link(); ?>#section-progress" class="btn btn-yellow" ><?php echo  __( 'УЧАСТВОВАТЬ', 'preico' );?></a>
						<a href="<?php echo site_url(); ?><?php echo wpml_site_link(); ?>calculator" class="btn btn-club margin-0" ><?php echo  __( 'КАЛЬКУЛЯТОР', 'preico' );?></a>
					</div>

				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-12 right-side">
					<div class="hidden-xs">
						<img class="circle" src="<?php bloginfo('template_directory');?>/img/circle2.png">
						<div class="titles">
							<div class="top-first">
								<p class="first-title"><?php echo  __( 'Когенерационная станция<br>электрическая мощность 10 МВТ<br>тепловая мощность 27,7 МВТ', 'preico' );?></p>
							</div>
							<div class="top-second">
								<p class="second-title"><?php echo  __( 'Завод древесных пеллет<br>мощность 120 000 тонн/год', 'preico' );?></p>
								<p class="third-title"><?php echo  __( 'Дата - Центр для Mайнинга<br>10 МВТ 84375 TH/s', 'preico' );?></p>
							</div>
							<div class="bottom-first">
								<p class="fourth-title"><?php echo  __( 'Лесозавод<br>мощность 120 000 м3/год', 'preico' );?></p>
								<p class="fifth-title"><?php echo  __( 'Tепличный комплекс<br>1 млн саженцев ели/год', 'preico' );?></p>
							</div>
							<div class="bottom-second">
								<p class="sixth-title"><?php echo  __( 'Лесные ресурсы - площадь лесного фонда 71 967 га', 'preico' );?></p>
							</div>
						</div>
					</div>
					
					<div class="details-sm visible-xs">
						<div>
							<img src="<?php bloginfo('template_directory');?>/resources/images/wood.png">
							<p><?php echo  __( 'Лесные ресурсы - площадь лесного фонда 71 967 га', 'preico' );?></p>
							<img class="bullet" src="<?php bloginfo('template_directory');?>/resources/images/yellow-arrow-down.png">
						</div>

						<div>
							<img src="<?php bloginfo('template_directory');?>/resources/images/factory-2.png">
							<p><?php echo  __( 'Лесозавод<br>мощность 120 000 м3/год', 'preico' );?></p>
							<img class="bullet" src="<?php bloginfo('template_directory');?>/resources/images/yellow-arrow-down.png">
						</div>

						<div>
							<img src="<?php bloginfo('template_directory');?>/resources/images/factory.png">
							<p><?php echo  __( 'Завод древесных пеллет<br>мощность 120 000 тонн/год', 'preico' );?></p>
							<img class="bullet" src="<?php bloginfo('template_directory');?>/resources/images/yellow-arrow-down.png">
						</div>

						<div>
							<img src="<?php bloginfo('template_directory');?>/resources/images/electr-factory.png">
							<p><?php echo  __( 'Когенерационная станция<br>электрическая мощность 10 МВТ<br>тепловая мощность 27,7 МВТ', 'preico' );?></p>
							<img class="bullet" src="<?php bloginfo('template_directory');?>/resources/images/yellow-arrow-down.png">
						</div>

						<div>
							<img src="<?php bloginfo('template_directory');?>/resources/images/farm.png">
							<p><?php echo  __( 'Дата - Центр для Mайнинга<br>10 МВТ 84375 TH/s', 'preico' );?></p>
							<img class="bullet" src="<?php bloginfo('template_directory');?>/resources/images/yellow-arrow-down.png">
						</div>

						<div>
							<img src="<?php bloginfo('template_directory');?>/resources/images/warmhouse.png">
							<p><?php echo  __( 'Tепличный комплекс<br>1 млн саженцев ели/год', 'preico' );?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section><!-- /.section-top -->

	<!-- Section timer -->
	<section class="section-timer">
		<div class="container">
			<div class="row">
				<h3><?php echo  __( 'До конца PRE ICO', 'preico' );?></h3>
				
				<div class="counter-wrap">
					<div class="days dark-bg">
						<div class="counter-number">00</div>
						<div class="counter-text" style="display: block;"><?php echo  __( 'Days', 'preico' );?></div>
					</div>
					<div class="hours dark-bg">
						<div class="counter-number" style="display: block;">00</div>
						<div class="counter-text" style="display: block;"><?php echo  __( 'Hours', 'preico' );?></div>
					</div>
					<div class="minutes dark-bg">
						<div class="counter-number" style="display: block;">00</div>
						<div class="counter-text" style="display: block;"><?php echo  __( 'Minutes', 'preico' );?></div>
					</div>
					<div class="seconds dark-bg">
						<div class="counter-number" style="display: block; opacity: 0.15964;">00</div>
						<div class="counter-text" style="display: block;"><?php echo  __( 'Seconds', 'preico' );?></div>
					</div>
				</div>
			</div>
		</div>
	</section><!-- /.section-timer -->
	
	<!-- Section Progress -->
	<section class="section-progress" id="section-progress">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12 left-prog">
					<div class="top-title">
						<p class="first"><?php echo  __( 'Soft Cap', 'preico' );?></p>
						<p class="second"><?php echo  __( '250 000 EUR', 'preico' );?></p>
					</div>
					<div class="progress">
						<img class="progress-bg" src="<?php bloginfo('template_directory');?>/img/progress.png">
						<svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="373" xmlns="http://www.w3.org/2000/svg">
							<circle class="circle-chart__background" stroke="#46401c" stroke-width="1" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
							<circle class="circle-chart__circle" stroke="#ffd334" stroke-width="1" stroke-dasharray="0,100" stroke-linecap="" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
						</svg>
					</div>
					<div class="bottom-title">
						<p class="first"><?php echo  __( 'Hard Cap', 'preico' );?></p>
						<p class="second"><?php echo  __( '1 000 000 EUR', 'preico' );?></p>
						<p class="curently-recived"><?php echo  __( 'Получено инвестиций', 'preico' );?> <span>0.000 ETH</span></p>
						<p class="curently-recived-value"><span class="value">0 EUR</span> <?php echo  __( 'на', 'preico' );?> <span class="time">02.04.2018 00:00 MSK (GMT+3)</span></p>
					</div>
					
				</div>
			
				<div class="col-md-6 col-sm-6 col-xs-12 right-prog">
					<p><?php echo  __( 'Чтобы стать членом клуба инвестора WPClub вы должны приобрести не менее 500 токенов. Какие ждут привилегии для участников клуба, вы можете ознакомиться на', 'preico' );?> <a class="wallet-desc-link" href="<?php echo site_url(); ?><?php echo wpml_site_link(); ?>club/"><?php echo  __( 'странице клуба', 'preico' );?></a></p>
					<label for="calc"><?php echo  __( 'Введите желаемую сумму токенов:', 'preico' );?></label>
					<form class="form-inline">
						<div class="form-group">
						  <input type="text"  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/^0+/, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" class="form-control" id="calc" value="100">
						</div>
						<div class="mobile-wrap">
							<button class="btn btn-success btn-green calculate mobile-inline"><?php echo  __( 'Расcчитать', 'preico' );?></button>
							<div class="form-group mobile-inline">
								<p><?php echo  __( 'СКИДКА 75%', 'preico' );?></p>
							</div>
						</div>
					</form>
					<div class="mobile-align">
						<p class="eth-price"><?php echo  __( 'Стоимость', 'preico' );?> 1 ETH = <span class="eth_txt"><span class="value">0 EUR</span> <?php echo  __( 'на', 'preico' );?> <span class="time">02.04.2018 00:00 MSK (GMT+3)</span></span></p>
						<p class="must_pay"><?php echo  __( 'Вы должны оплатить', 'preico' );?> <span>0.00000000 ETH (0.00 EUR)</span></p>
						
						<p class="wallet"><?php echo  __( 'Номер контракта', 'preico' );?></p>
						<button class="btn btn-success btn-green copy" data-clipboard-action="copy" data-clipboard-target="#wallet-value" onclick="copyToClipboard('.important-message')"><?php echo  __( 'Копировать', 'preico' );?></button>
						<button class="btn btn-success btn-green openqr" data-toggle="modal" data-target="#qrModal"><?php echo  __( 'Oткрыть QR код', 'preico' );?></button>
						<input id="wallet-value" class="wallet-value important-message" value="0xD67aA6a98e99F979F23bF0da772d113fE6DBe50a">
						<a href="<?php the_field('main_etherium_wallet'); ?>" class="wallet-desc-link" target="_blank"><?php echo  __( 'Инструкция "Как создать эфир кошелек?"', 'preico' );?></a>
						<br>
						<a href="<?php the_field('main_terms'); ?>" class="wallet-desc-link" target="_blank"><?php echo  __( 'Условия и Положения!', 'preico' );?></a>
						
						<p class="wallet-desc-title"><?php echo  __( 'Для инвестирования, отправьте Эфир на официальный смарт контракт краудсейла.Нельзя переводить с обменников, только ETH кошельки. При транзакции, пожалуйста, ставте GAS LIMIT 200000.', 'preico' );?></p>
					</div>
					<div class="divided-blocks">
						<div class="col-md-6 col-sm-12 col-xs-12 clear-pads border-right">
							<p class="font20"><?php echo  __( 'Soft Cap', 'preico' );?><br><span class="bold"><?php echo  __( '0 - 250 000 EUR', 'preico' );?></span></p>
							<div class="col-md-12 clear-pads opts">
								<div class="black yellow-bg bold"><?php echo  __( 'СКИДКА 75%', 'preico' );?></div>
							</div>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12 clear-pads no-pad-right">
							<p class="font20"><?php echo  __( 'Hard Cap', 'preico' );?><br><span class="bold"><?php echo  __( '250 000 - 1 000 000 EUR', 'preico' );?></span></p>
							<div class="col-md-12 clear-pads opts">
								<div class="black yellow-bg bold"><?php echo  __( 'СКИДКА 50%', 'preico' );?></div>
							</div>
						</div>
					</div>
					<div class="token-wrap">
						<p class="token-title"><?php echo  __( 'Обратный выкуп по регламенту компании', 'preico' );?></p>
						<p class="token">1 Euro = <span>1 WPC</span></p>
					</div>
				</div>
				
				<div class="modal fade" id="qrModal" role="dialog">
					<div class="modal-dialog" style="width:265px;">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-body">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<br>
								<img src="<?php bloginfo('template_directory');?>/img/qr-crowdsale.png">
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section><!-- /.section-progress -->
	
	<!-- Section Problem -->
	<section class="section-problem padding-t-10">
		<div class="container">
			<div class="row">
				
				<?php  
					if (ICL_LANGUAGE_CODE === 'ru')
					{
						$mypage = get_post(318);
					}
					else if (ICL_LANGUAGE_CODE === 'en') 
					{
						$mypage = get_post(320);
					}
					else 
					{
						$mypage = get_post(320);
					}	
				?>
			
				<h3 class="title"><?php echo $mypage->post_title; ?></h3>
				
				<?php echo $mypage->post_content; ?>
			</div>
		</div>
	</section>
	
	<section id="project">
		<div class="container">
		
			<?php  
				if (ICL_LANGUAGE_CODE === 'ru')
				{
					$mypage = get_post(325);
				}
				else if (ICL_LANGUAGE_CODE === 'en') 
				{
					$mypage = get_post(328);
				}
				else 
				{
					$mypage = get_post(328);
				}	
			?>
		
			<h3 class="title"><?php echo $mypage->post_title; ?></h3>
			<div class="col-md-6 col-sm-12 col-xs-12">
				<!--<a href="" class="" data-toggle="modal" data-target="#videoModal" > -->

					<div class="col-md-12 clear-pads video-preview" style="background: url(<?php bloginfo('template_directory');?>/img/video.png)no-repeat center center;
						-webkit-background-size: cover;
						-moz-background-size: cover;
						-o-background-size: cover;
						background-size: cover;">

					</div>
				<!--</a> -->


				<div class="modal fade" id="videoModal" role="dialog">
					<div class="modal-dialog">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-body"><button type="button" class="close" data-dismiss="modal">&times;</button>
								<iframe width="560" height="315" src="https://www.youtube.com/embed/dE5j709dm_g" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
								
								<!--<iframe

									src="<?php echo get_field( 'video_url' );?>"
									frameborder="0" gesture="media"
									allowfullscreen></iframe>-->
							</div>
						</div>

					</div>
				</div>
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12 text-block no-margin-padding">
				<?php echo $mypage->post_content; ?>
			</div>

			
		</div>

	</section>
	
	<section class="section-problem">

		<div class="container">
			<div class="row">
				
				<div class="col-md-12 col-sm-12 col-xs-12 clear-pads posts-wrap ">	
				
					<?php
						$args = array(
							'post_type'      => 'feature',
							'post_status'    => 'publish',
							'orderby'        => 'post_date',
							'order'          => 'ASC',
							'posts_per_page' => '8'
						);
						$posts = new WP_Query( $args );

						if ( $posts->have_posts() ) : ?>
				
							<?php while ( $posts->have_posts() ) : $posts->the_post(); ?>
							
								<?php $image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() )); ?>
							
								<div class="col-md-3 col-sm-6 post-wrap">
									<div>
										<div class="description-post-img" style="background: url(<?php echo $image; ?>)no-repeat center center;
												-webkit-background-size: contain;
												-moz-background-size: contain;
												-o-background-size: contain;
												background-size: contain;">
										</div>
									</div>
									<h4><?php the_title(); ?></h4>
									<div class="posts-desc">
										<p><?php the_content(); ?></p>
									</div>
								</div>

							<?php endwhile; ?>
						<?php wp_reset_query(); ?>
					<?php endif; ?>
				</div>
		
			</div>
		</div>
		
		<div class="container-fluid margin-t-20">
			<div class="row">
				<div class="city-image col-md-12 col-sm-12 col-xs-12 clear-pads hidden-xs"></div>
			</div>
		</div>
		
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12 clear-pads">
				
					<?php
						$args = array(
							'post_type'      => 'infographic',
							'post_status'    => 'publish',
							'orderby'        => 'post_date',
							'order'          => 'ASC',
							'posts_per_page' => '6'
						);
						$posts = new WP_Query( $args );
						
						$num = 0;

						if ( $posts->have_posts() ) : ?>
				
							<?php while ( $posts->have_posts() ) : $posts->the_post(); ?>
							
								
								<div class="col-md-2 col-sm-4 col-xs-12 full-width-xs">
									<div class="dark-bg triangle-top" data-toggle="modal" data-target="#infoModal<?php echo $num; ?>">
										<h4 class="yellow"><?php the_title(); ?></h4>
										<p class="white"><?php the_field('info-desc'); ?></p>
									</div>

									<div class="modal fade" id="infoModal<?php echo $num; ?>" role="dialog">
										<div class="modal-dialog">
											<!-- Modal content-->
											<div class="modal-content">
												<div class="modal-body"><button type="button" class="close" data-dismiss="modal">×</button>
													<h4 class="yellow"><?php the_title(); ?></h4>
													<p class="white"><?php the_content(); ?><p/>
												</div>
											</div>
										</div>
									</div>
								</div>
	
							<?php 
								$num++;
								endwhile; 
							?>
						<?php wp_reset_query(); ?>
					<?php endif; ?>
				
				</div>
				
			</div>
		</div>
		
	</section><!-- /.section-problem -->
	
	<!-- Section Club -->
	<section class="section-club">
		<div class="container">
			<div class="row">
				<h3 class="title white"><?php echo  __( 'Программа участия в клубе WP', 'preico' );?></h3>
				
				<div class="club-wrap">
				
					<?php
						$args = array(
							'post_type'      => 'club',
							'post_status'    => 'publish',
							'orderby'        => 'post_date',
							'order'          => 'ASC',
							'posts_per_page' => '4'
						);
						$posts = new WP_Query( $args );
						
						$num = 0;

						if ( $posts->have_posts() ) : ?>
				
							<?php while ( $posts->have_posts() ) : $posts->the_post(); ?>
							
								<?php $image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() )); ?>
							
								<div class="col-md-3 col-sm-6 col-xs-12 center border-club" data-toggle="modal" data-target="#clubModal<?php echo $num; ?>">
									<img src="<?php echo $image; ?>">
									
									<p class="club-wp-desc"><?php the_field('token-value'); ?></p>
								</div>

								<div class="modal fade" id="clubModal<?php echo $num; ?>" role="dialog">
									<div class="modal-dialog">
										<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-body"><button type="button" class="close" data-dismiss="modal">×</button>
												<h4 class="yellow"><?php the_title(); ?></h4>
												<p class="white"><?php the_content(); ?></p>
												<p class="yellow"><?php the_field('bonus'); ?></p>
											</div>
										</div>
									</div>
								</div>
	
							<?php 
								$num++;
								endwhile; 
							?>
						<?php wp_reset_query(); ?>
					<?php endif; ?>
				
				</div>
				
				<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 club-text">
					<p><?php echo  __( 'Инвестируй в реальный бизнес и получай процент от дохода предприятий, ознакомиться с условиями клубной программы можно на странице WPC клуб.', 'preico' );?></p>
				</div>
				
				<div class="center club-buttons">
					<a href="<?php echo site_url(); ?><?php echo wpml_site_link(); ?>club/" class="btn btn-club"><?php echo  __( 'ОЗНАКОМИТЬСЯ', 'preico' );?></a>
					<a href="<?php echo site_url(); ?><?php echo wpml_site_link(); ?>registration/" class="btn btn-yellow"><?php echo  __( 'РЕГИСТРАЦИЯ', 'preico' );?></a>
				</div>	
			</div>
		</div>
	</section><!-- /.section-club -->
	
	<section id="news">
		<div class="container border-bottom">

			<h3 class="title"><?php echo __( 'Новости', 'preico' ) ?></h3>
			
			<?php
				$args = array(
					'post_type'      => 'post',
					'category_name'  => 'news',
					'post_status'    => 'publish',
					'orderby'        => 'post_date',
					'order'          => 'DESC',
					'posts_per_page' => '4'
				);
				$posts = new WP_Query( $args );

				if ( $posts->have_posts() ) : ?>

					<?php $num = 0; ?>
						<?php while ( $posts->have_posts() ) : $posts->the_post(); ?>

							<div class="col-md-3 col-sm-6 col-xs-12 team-member" style="display: block;">
							<?php $image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() )); ?>
							
								<?php if($image) { ?>
									<img src="<?php echo $image; ?>" class="img-thumbnail" width="250" height="150">
								<?php } else { ?>
									<img src="<?php bloginfo('template_directory');?>/img/default.png" class="img-thumbnail" width="250" height="150">
								<?php } ?>

								<a href="<?php the_permalink(); ?>" class=""><?php the_title(); ?></a>
								<div class=""><?php the_content(); ?></div>
							</div>

						<?php $num++;?>
					<?php endwhile; ?>
				<?php wp_reset_query(); ?>
			<?php endif; ?>
			
			<div class="col-md-12 col-sm-12 col-xs-12 clear-pads text-center collapse-btn-container margin-t-20 margin-b-40">
				<a href="<?php echo site_url(); ?><?php echo wpml_site_link(); ?>news" class="btn more-btn btn-club" role="button">
					<span class="more"><?php echo __( 'Новости', 'preico' ) ?></span>
				</a>
			</div>
			
		</div>
	</section>
	
	<!-- Section Subscribe -->
	<section class="section-subscribe">
		<div class="container">
			<div class="row">
				<div class="subscription-container">

					<div class="col-md-6 col-sm-6 subscribe-description ">
						<div class="col-md-3 col-sm-12 clear-pads mail-img">
							<img src="<?php bloginfo('template_directory');?>/resources/images/mail.png">
						</div>
						<div class="col-md-9 col-sm-12">
							<h1><?php echo __( 'Подпишитесь', 'preico' ) ?></h1>
							<p><?php echo __( 'еженедельная подпискa новостей.
								Информация, которая будет отсылаться
								при подписке.', 'preico' ) ?>
							</p>
						</div>

					</div>
					<div class="col-md-6 col-sm-6">
						<?php es_subbox( $namefield = "YES", $desc = "", $group = "Public" ); ?>
					</div>
				</div>
			</div>
		</div>
	</section><!-- /.section-subscribe -->
	
	
	<section id="roadmap">
		<h1 class="title white"><?php echo __( 'Дорожная карта', 'preico' ) ?></h1>

		<div class="container">
			<div class="col-md-6 col-sm-6 left-block">
				<div class="roadmap-block first">
					<h4 class="yellow"><?php echo __( '2017 Q2-Q4 - Разработка проектной базы', 'preico' ) ?></h4>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/peregovor.png">

						</div>
						<p class="title-text"><?php echo __( 'Проведение переговоров с поставщиками', 'preico' ) ?></p>
					</div>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/zapusk_sajta.png">

						</div>
						<p class="title-text"><?php echo __( 'Запуск сайта', 'preico' ) ?></p>
					</div>
				</div>

				<div class="roadmap-block second hidden-lg hidden-md hidden-sm">
					<h4 class="yellow"><?php echo __( '2018 Q1  - PRE SALE', 'preico' ) ?></h4>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/marketing.png">

						</div>
						<p class="title-text"><?php echo __( 'Маркетинговая составляющая', 'preico' ) ?></p>
					</div>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/registracija.png">

						</div>
						<p class="title-text"><?php echo __( 'Регистрационные процессы', 'preico' ) ?></p>
					</div>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/video.png">

						</div> <p class="title-text"><?php echo __( 'Установка видеонаблюдения', 'preico' ) ?></p>
					</div>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/rabota_s_investorami.png">

						</div><p class="title-text"><?php echo __( 'Работа с инвесторами', 'preico' ) ?></p>
					</div>
				</div>

				<div class="roadmap-block third">
					<h4 class="yellow"><?php echo __( '2018 Q2 - ICO', 'preico' ) ?></h4>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/podgotovitelnie_raboti.png">

						</div>
						<p class="title-text"><?php echo __( 'Подготовительные работы', 'preico' ) ?></p>
					</div>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/pokupka.png">

						</div>
						<p class="title-text"><?php echo __( 'Покупка техники', 'preico' ) ?></p>
					</div>
					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/stroitelstvo.png">

						</div><p class="title-text"><?php echo __( 'Начало строительных работ', 'preico' ) ?></p>

					</div>
					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/pokupka.png">

						</div>
						<p class="title-text"><?php echo __( 'Покупка производственного оборудования', 'preico' ) ?></p>
					</div>
				</div>

			</div>

			<div class="col-md-6 col-sm-6 right-block hidden-xs">
				<div class="roadmap-block second">
					<h4 class="yellow"><?php echo __( '2018 Q1  - PRE SALE', 'preico' ) ?> </h4>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/marketing.png">

						</div>
						<p class="title-text"><?php echo __( 'Маркетинговая составляющая', 'preico' ) ?></p>
					</div>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/registracija.png">

						</div>
						<p class="title-text"><?php echo __( 'Пегистрационные процессы', 'preico' ) ?></p>
					</div>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/video.png">

						</div> <p class="title-text"><?php echo __( 'Установка видеонаблюдения', 'preico' ) ?></p>
					</div>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/rabota_s_investorami.png">

						</div><p class="title-text"><?php echo __( 'Работа с инвесторами', 'preico' ) ?></p>
					</div>
				</div>
			</div>

			<div class="col-md-12 col-sm-12 bottom-block">
				<div class="roadmap-block fourth">
					<h4 class="yellow"><?php echo __( '2019 Q2-Q4 - Запуск комплекса', 'preico' ) ?></h4>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/nachalo_prodaz.png">

						</div><p class="title-text"><?php echo __( 'Начало продаж продукции компании', 'preico' ) ?></p>

					</div>

					<div class="roadmap-item">
						<div class="li-img-wrap">
							<img src="<?php bloginfo('template_directory');?>/resources/images/token.png">

						</div><p class="title-text"><?php echo __( 'Начало выкупа токенов', 'preico' ) ?></p>

					</div>
				</div>
			</div>

		</div>

	</section>
	
	<section id="contact">
		<div class="container border-bottom">

			<h3 class="title"><?php echo __( 'Свяжитесь с нами', 'preico' ) ?></h3>
			
			<div class="col-md-6 col-sm-12 col-xs-12 clear-pads form">
			
				<?php  
					if (ICL_LANGUAGE_CODE === 'ru')
					{
						echo do_shortcode( '[contact-form-7 id="64" title="Связь с нами"]' );
					}
					else if (ICL_LANGUAGE_CODE === 'en') 
					{
						echo do_shortcode( '[contact-form-7 id="525" title="Связь с нами_copy"]' );
					}
					else 
					{
						echo do_shortcode( '[contact-form-7 id="525" title="Связь с нами_copy"]' );
					}	
				?>
			</div> 
			
			<div class="col-md-6 col-sm-12 col-xs-12 contactinfo">
				<div class="col-md-12 col-sm-6 clear-pads">
                    <div class="address">

                        <img class="address-icon" src="<?php bloginfo('template_directory');?>/resources/images/icons/address.png">
                        <p><?php echo __( '182250, Псковская обл., Себежский р-н г. Себеж, ул. В.И. Марго, д. 32', 'preico' ) ?></p>
                    </div>
					<div class="ap-mail">

                        <img class="phone-icon" src="<?php bloginfo('template_directory');?>/resources/images/icons/ap-mail.png">
                        <p>info@woodpellets.io</p>
                    </div>
                    <div class="phone">

                        <img class="phone-icon" src="<?php bloginfo('template_directory');?>/resources/images/icons/phone.png">
                        <p><?php echo get_option( 'phone_ico' ); ?></p>
                    </div>
                </div>

                <div class="col-md-12 col-sm-6 clear-pads soc-icons">
					<a target="_blank" href="<?php echo get_option( 'vk_link_ico' ); ?>" class="soc vk soc2">
						<i class="fa fa-vk" aria-hidden="true"></i>
					</a>
					<a target="_blank" href="<?php echo get_option( 'fb_link_ico' ); ?>" class="soc fb soc2">
						<i class="fa fa-facebook" aria-hidden="true"></i>
					</a>
					<a target="_blank" href="<?php echo get_option( 'tl_link_ico' ); ?>" class="soc tl soc2">
						<i class="fa fa-paper-plane" aria-hidden="true"></i>
					</a>
					<a target="_blank" href="<?php echo get_option( 'tw_link_ico' ); ?>" class="soc tl soc2">
						<i class="fa fa fa-twitter" aria-hidden="true"></i>
					</a>
					<a target="_blank" href="<?php echo get_option( 'md_link_ico' ); ?>" class="soc tl soc2">
						<i class="fa fa-medium" aria-hidden="true"></i>
					</a>

                    <p class="margin-t-20"> <?php echo  __( 'Наши страницы в социальных сетях', 'preico' );?> </p>
                </div>
			</div> 
		</div>
	</section>
	
	<section id="team">
		<div class="container border-bottom">

			<h3 class="title"><?php echo __( 'Наша команда', 'preico' ) ?></h3>
			
			<?php
				$args = array(
					'post_type'      => 'team',
					'post_status'    => 'publish',
					'orderby'        => 'post_date',
					'order'          => 'ASC',
					'posts_per_page' => '4'
				);
				$posts = new WP_Query( $args );

				if ( $posts->have_posts() ) : ?>

						<?php while ( $posts->have_posts() ) : $posts->the_post(); ?>

							<div class="col-md-3 col-sm-6 col-xs-12 team-member team-member-block" style="display: block;">
							<?php $image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() )); ?>
								<?php if($image) { ?>
									<img src="<?php echo $image; ?>" class="img-circle" width="180" height="180">
								<?php } else { ?>
									<img src="<?php bloginfo('template_directory');?>/img/people.png" class="img-circle" width="180" height="180">
								<?php } ?>
								<h4 class=""><?php the_title(); ?></h4>
								<div id="team-desc" class="hide"><?php the_content(); ?></div>
							</div>

					<?php endwhile; ?>
				<?php wp_reset_query(); ?>
			<?php endif; ?>
			
			<?php
				$args = array(
					'post_type'      => 'team',
					'post_status'    => 'publish',
					'orderby'        => 'post_date',
					'order'          => 'ASC',
					'posts_per_page' => '250',
					'offset'		 => '4'
				);
				$posts = new WP_Query( $args );

				if ( $posts->have_posts() ) : ?>

			<?php while ( $posts->have_posts() ) : $posts->the_post(); ?>

					<div class="col-md-3 col-sm-6 col-xs-12 team-member team-member-block" style="display: none;">
						<?php $image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() )); ?>
						<?php if($image) { ?>
							<img src="<?php echo $image; ?>" class="img-circle" width="180" height="180">
						<?php } else { ?>
							<img src="<?php bloginfo('template_directory');?>/img/people.png" class="img-circle" width="180" height="180">
						<?php } ?>
						<h4 class=""><?php the_title(); ?></h4>
						<div id="team-desc" class="hide"><?php the_content(); ?></div>
					</div>

				<?php endwhile; ?>
				<?php wp_reset_query(); ?>
			<?php endif; ?>
				
				
			<div class="col-md-12 col-sm-12 col-xs-12 clear-pads text-center collapse-btn-container margin-b-40">
				<a class="btn more-btn btn-club" role="button">
					<span class="more"><?php echo __( 'Показать', 'preico' ) ?></span>
					<span class="less"><?php echo __( 'Скрыть', 'preico' ) ?></span>
				</a>
			</div>
		</div>
		
		
		
	</section>
	
	<section id="partners">
		<div class="container">
			<h3 class="title"><?php echo __( 'Партнеры', 'preico' ) ?></h3>

			<?php
			$args = array(
				'post_type'      => 'partner',
				'post_status'    => 'publish',
				'orderby'        => 'post_date',
				'order'          => 'DESC',
				'posts_per_page' => '-1'
			);
			$posts = new WP_Query( $args );

			if ( $posts->have_posts() ) : ?>
				<div class="partners-wrap col-md-12 clear-pads">
					<?php while ( $posts->have_posts() ) : $posts->the_post(); ?>

						<?php $image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() )); ?>
						<?php if($image) { ?>
							<div class="partner col-md-3 clear-pads" style="background: url(<?php echo $image; ?>)no-repeat center center;
								-webkit-background-size: contain;
								-moz-background-size: contain;
								-o-background-size: contain;
								background-size: contain;">
						<?php } ?>

						</div>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>

		</div>
	</section>
	
	<div id="scrollup" class="">
		<a href="#wpclub"></a>
	</div>
	
<?php get_footer(); ?>


