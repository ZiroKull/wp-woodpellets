<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
	
	<!-- Section Club Top -->
	<section class="section-club-top section-news">
		<div class="container">
			<div class="row">
				<div class="col-md-7 col-sm-6 ">
					<h1><?php echo __( 'Новости', 'preico' ) ?></h1>
					<p><?php echo __( 'В данной категории можно ознакомится с событиям происходящими в индустрии деревообработки, зеленой энергетики и майнинга', 'preico' ) ?></p>
				</div>
			</div>
		</div>
	</section><!-- /.section-club-top -->

	<section class="section-content section-post">
		<div class="container">
			<div class="row club-row-top">
				<div class="col-md-12">
					<div id="primary" class="content-area">
						<main id="main" class="site-main" role="main">

						<?php
						// Start the loop.
						while ( have_posts() ) : the_post();

							/*
							 * Include the post format-specific template for the content. If you want to
							 * use this in a child theme, then include a file called content-___.php
							 * (where ___ is the post format) and that will be used instead.
							 */
							get_template_part( 'content', get_post_format() );

						// End the loop.
						endwhile;
						?>
						
							<div class="readmore margin-t-20">
								<a href="<?php echo site_url(); ?><?php echo wpml_site_link(); ?>news/"><?php echo __( 'Назад к Новостям', 'preico' ) ?></a>
							</div>

						</main><!-- .site-main -->
					</div><!-- .content-area -->
				</div>
				
			</div>
		</div>
	</section>

<?php get_footer(); ?>
