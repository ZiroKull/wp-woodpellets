<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<!-- Fixed navbar -->
	<nav id="header" class="navbar navbar-fixed-top">
		<div id="header-container" class="container navbar-container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a id="brand" class="navbar-brand" href="<?php echo site_url(); ?>">
					<img src="<?php bloginfo('template_directory');?>/img/logo.png" alt="">
				</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse pull-right-lg">
				<ul class="nav navbar-nav">
					<li class="active"><a href="<?php echo site_url(); ?>/calculator">Калькулятор</a></li>
					<li><a href="<?php echo site_url(); ?>/documents">Документация</a></li>
					<li><a href="<?php echo site_url(); ?>/#team">Команда</a></li>
					<li><a href="<?php echo site_url(); ?>/questions">Вопросы</a></li>
					<li><a href="<?php echo site_url(); ?>/#contact">Контакты</a></li>
					
					<?php if( is_user_logged_in() ){ ?>
						<li><a href="<?php echo site_url(); ?>/private-office/">Кабинет</a></li>
					<?php }else { ?>
						<li><a href="<?php echo site_url(); ?>/registration/">Регистрация</a></li>
						<li><a href="<?php echo site_url(); ?>/login/">Кабинет</a></li>
					<?php } ?>
					
					<!--
					<li>
						<div class="dropdown">
							<button class="dropdown-toggle" type="button" data-toggle="dropdown">
								<span class="locale">RU</span>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="">RU</a></li>
								<li><a href="">EN</a></li>
							</ul>
						</div>
					</li>-->
				</ul>
			</div><!-- /.nav-collapse -->
		</div><!-- /.container -->
	</nav><!-- /.navbar -->

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		// End the loop.
		endwhile;
		?>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
